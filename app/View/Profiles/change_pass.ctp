<?php echo $this->Session->flash();?>
<div class="row">
    <div class="col-md-2">
		<div class="panel panel-default">
		<div class="panel-heading">
			<div class="widget">
				<h4 class="widget-title">My <span>Profile</span></h4>
			</div>
		</div>
		<div class="list-group">
                        <?php echo$this->Html->link("Edit Profile",array('controller'=>'Profiles','action'=>'editProfile'),array('class'=>'list-group-item'));?>
			<?php echo$this->Html->link("Change Photo",array('controller'=>'Profiles','action'=>'changePhoto'),array('class'=>'list-group-item'));?>
			<?php echo$this->Html->link('Change Password',array('controller'=>'Profiles','action'=>'changePass'),array('class'=>'list-group-item active'));?>			
		</div>
		</div>
	</div>
    <div class="col-md-10">  
        <div class="panel panel-default">
            <div class="panel-heading">
			<div class="widget">
				<h4 class="widget-title">Change <span>Password</span></h4>
			</div>
		</div>            
                <div class="panel-body">
                <?php echo $this->Form->create('Profile', array( 'controller' => 'Profiles', 'action' => 'changePass','name'=>'post_req','id'=>'post_req','class'=>'form-horizontal'));?>
                     <div class="form-group">
                        <label for="group_name" class="col-sm-3 control-label">Old Password</label>
                        <div class="col-sm-9">
                           <?php echo $this->Form->input('oldPassword',array('type'=>'password','id'=>'oldPassword','value'=>'','label' => false,'class'=>'form-control input-sm validate[required,minSize[4],maxSize[15]]','placeholder'=>'Old Password','div'=>false));?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="group_name" class="col-sm-3 control-label">Password</label>
                        <div class="col-sm-9">
                           <?php echo $this->Form->input('password',array('id'=>'password','value'=>'','label' => false,'class'=>'form-control input-sm validate[required,minSize[4],maxSize[15]]','placeholder'=>'Password','div'=>false));?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="group_name" class="col-sm-3 control-label">Confirm Password</label>
                        <div class="col-sm-9">
                           <?php echo $this->Form->input('con_password',array('type'=>'password','value'=>'','label' => false,'class'=>'form-control input-sm validate[required,minSize[4],maxSize[15],equals[password]]','placeholder'=>'Confirm Password','div'=>false));?>
                        </div>
                    </div>
                    <div class="form-group text-left">
                        <div class="col-sm-offset-3 col-sm-10">
                            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-refresh"></span> Update</button>                            
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
    </div>
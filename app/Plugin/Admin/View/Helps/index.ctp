<div class="row">
    <div  class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
			<div class="widget">
				<h4 class="widget-title"> <span>Help</span></h4>
			</div>
		</div>
            <div class="cust-supt">CUSTOMER SUPPORT</div>
            <div class="row">
                <div class="col-md-7"><div class="cust-head2">A Responsive and Dedicated Technical Support, Any where, Any time.</div>
                    <ul class="cust-list">
                        <li><span>Forum</span> -- (24 Hours)</li>
                        <li><span>Email Support</span> -- (24 Hours)</li>
                        <li><span>Live Chat</span> -- (10:30 AM to 8.00 PM (GMT +5:30, INDIA))</li>
                        <li><span>Telephonic Support</span> -- (10:30 AM to 6.00 PM (GMT +5:30, INDIA))</li>
                        <li><?php echo$this->Html->Link('Documentation','http://www.eduexpression.com/support/',array('target'=>'_blank'));?></li>
                        <li><?php echo$this->Html->Link('Setup Guide','http://www.eduexpression.com/quickstart/',array('target'=>'_blank'));?></li>
                        <li><?php echo$this->Html->Link('Videos','http://www.eduexpression.com/videos/',array('target'=>'_blank'));?></li>
                        <li><?php echo$this->Html->Link('For support Email at support@eduexpression.com','mailto:support@eduexpression.com');?></li>
                        <li><?php echo$this->Html->Link('Credit Note','http://www.eduexpression.com/credit/',array('target'=>'_blank'));?></li>
                    </ul>
                </div>
                <div class="col-md-5">
                    <?php echo$this->Html->image('customer-support.png',array('class'=>'img-rounded'));?>
                </div>
            </div>
        </div>
    </div>
</div>
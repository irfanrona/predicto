-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 19, 2014 at 11:30 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `examsoft`
--

-- --------------------------------------------------------

--
-- Table structure for table `configurations`
--

DROP TABLE IF EXISTS `configurations`;
CREATE TABLE IF NOT EXISTS `configurations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `organization_name` varchar(255) NOT NULL,
  `domain_name` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `meta_title` text NOT NULL,
  `meta_desc` text NOT NULL,
  `timezone` varchar(100) NOT NULL,
  `author` varchar(255) NOT NULL,
  `sms` tinyint(1) NOT NULL,
  `email_notification` tinyint(1) NOT NULL,
  `guest_login` tinyint(1) NOT NULL,
  `front_end` tinyint(1) NOT NULL,
  `slides` tinyint(4) NOT NULL,
  `translate` tinyint(4) NOT NULL DEFAULT '0',
  `paid_exam` tinyint(4) NOT NULL DEFAULT '1',
  `leader_board` tinyint(1) NOT NULL DEFAULT '1',
  `contact` text NOT NULL,
  `photo` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `configurations`
--

INSERT INTO `configurations` (`id`, `name`, `organization_name`, `domain_name`, `email`, `meta_title`, `meta_desc`, `timezone`, `author`, `sms`, `email_notification`, `guest_login`, `front_end`, `slides`, `translate`, `paid_exam`,`leader_board`,`contact`, `photo`, `created`, `modified`) VALUES
(1, '{Site Name}', '{Site Name}', '', 'exam@demo.com', '{Site Name} Title', '{Site Name} Description', 'Asia/Calcutta', 'Exam Solution', 0, 0, 0, 0, 1, 0, 1,1,'0000-0000~info@eduexpression.com~http://facebook.com', '', '2014-04-08 20:56:04', '2014-05-12 11:24:25');

-- --------------------------------------------------------

--
-- Table structure for table `diffs`
--

DROP TABLE IF EXISTS `diffs`;
CREATE TABLE IF NOT EXISTS `diffs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `diff_level` varchar(15) NOT NULL,
  `type` char(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `diffs`
--

INSERT INTO `diffs` (`id`, `diff_level`, `type`) VALUES
(1, 'Easy', 'E'),
(2, 'Medium', 'M'),
(3, 'Difficult', 'D');

-- --------------------------------------------------------

--
-- Table structure for table `exams`
--

DROP TABLE IF EXISTS `exams`;
CREATE TABLE IF NOT EXISTS `exams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `instruction` text NOT NULL,
  `duration` int(11) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `passing_percent` int(11) NOT NULL,
  `negative_marking` varchar(3) NOT NULL,
  `attempt_count` int(11) NOT NULL,
  `declare_result` varchar(3) NOT NULL DEFAULT 'Yes',
  `finish_result` char(1) NOT NULL DEFAULT '0',
  `ques_random` char(1) NOT NULL DEFAULT '0',
  `paid_exam` char(1) NOT NULL DEFAULT '0',
  `amount` decimal(10,2) DEFAULT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'Inactive',
  `user_id` int(11) NOT NULL,
  `finalized_time` datetime DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exams`
--

INSERT INTO `exams` (`id`, `name`, `instruction`, `duration`, `start_date`, `end_date`, `passing_percent`, `negative_marking`, `attempt_count`, `declare_result`, `finish_result`, `ques_random`, `paid_exam`, `amount`, `status`, `user_id`, `finalized_time`, `created`, `modified`) VALUES
(1, 'Physics Exam', 'Please Read', 180, '2014-11-01 10:00:27', '2015-05-28 09:59:42', 40, 'Yes', 1, 'Yes', '0', '0', '0', NULL, 'Active', 0, NULL, '2014-04-26 23:10:31', '2014-04-26 23:11:51');

-- --------------------------------------------------------

--
-- Table structure for table `exam_groups`
--

DROP TABLE IF EXISTS `exam_groups`;
CREATE TABLE IF NOT EXISTS `exam_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `exam_id` (`exam_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `exam_groups`
--

INSERT INTO `exam_groups` (`id`, `exam_id`, `group_id`) VALUES
(1, 1, 6),
(2, 1, 1),
(3, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `exam_orders`
--

DROP TABLE IF EXISTS `exam_orders`;
CREATE TABLE IF NOT EXISTS `exam_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `exam_id` (`exam_id`),
  KEY `student_id` (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exam_questions`
--

DROP TABLE IF EXISTS `exam_questions`;
CREATE TABLE IF NOT EXISTS `exam_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `exam_id` (`exam_id`),
  KEY `question_id` (`question_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=231 ;

--
-- Dumping data for table `exam_questions`
--

INSERT INTO `exam_questions` (`id`, `exam_id`, `question_id`) VALUES
(228, 1, 3),
(229, 1, 2),
(230, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `exam_results`
--

DROP TABLE IF EXISTS `exam_results`;
CREATE TABLE IF NOT EXISTS `exam_results` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime DEFAULT NULL,
  `total_question` int(11) NOT NULL,
  `total_answered` int(11) NOT NULL,
  `total_marks` int(11) NOT NULL,
  `obtained_marks` decimal(18,2) NOT NULL,
  `result` varchar(10) NOT NULL,
  `percent` decimal(5,2) NOT NULL,
  `finalized_time` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `exam_id` (`exam_id`),
  KEY `student_id` (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exam_stats`
--

DROP TABLE IF EXISTS `exam_stats`;
CREATE TABLE IF NOT EXISTS `exam_stats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_result_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `ques_no` int(11) NOT NULL,
  `attempt_time` datetime DEFAULT NULL,
  `opened` char(1) NOT NULL DEFAULT '0',
  `answered` char(1) NOT NULL DEFAULT '0',
  `option_selected` varchar(15) NOT NULL,
  `answer` text NOT NULL,
  `true_false` varchar(5) NOT NULL,
  `fill_blank` varchar(100) NOT NULL,
  `marks` int(11) NOT NULL,
  `marks_obtained` decimal(5,2) NOT NULL,
  `closed` char(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `checking_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `exam_id` (`exam_id`),
  KEY `student_id` (`student_id`),
  KEY `question_id` (`question_id`),
  KEY `exam_result_id` (`exam_result_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(50) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_name` (`group_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `group_name`, `created`, `modified`) VALUES
(1, 'IIT', '2014-04-26 00:00:00', '2014-04-26 00:00:00'),
(2, 'PMT', '2014-03-25 18:30:44', '2014-04-26 00:00:00'),
(6, 'CPMT', '2014-04-18 16:50:38', '2014-04-18 16:50:38');

-- --------------------------------------------------------

--
-- Table structure for table `helpcontents`
--

DROP TABLE IF EXISTS `helpcontents`;
CREATE TABLE IF NOT EXISTS `helpcontents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link_title` varchar(255) NOT NULL,
  `link_desc` longtext NOT NULL,
  `status` varchar(8) NOT NULL DEFAULT 'Active',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `helpcontents`
--

INSERT INTO `helpcontents` (`id`, `link_title`, `link_desc`, `status`, `created`, `modified`) VALUES
(1, 'Help 1', '<p>Suspendisse mattis magna augue, sed pretium lacus pellentesque nec. Nullam tincidunt lacinia urna sit amet tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras consequat justo ac diam aliquet adipiscing. Ut orci nibh, viverra quis luctus id, lacinia quis purus. Vestibulum pharetra diam non nulla pretium scelerisque. Fusce posuere tellus vel mollis auctor.</p>', 'Active', '2014-05-12 11:35:34', '2014-05-12 11:35:34'),
(2, 'Help2', '<p>Aenean pretium nunc lectus, quis viverra metus accumsan vestibulum. Mauris vulputate urna nec leo viverra, at dictum nulla suscipit. Sed id pretium lectus, vitae egestas turpis. Quisque metus tortor, tristique in diam sit amet, suscipit facilisis augue. Nunc vel leo vitae ligula auctor tristique ut nec tortor. Aliquam nibh ligula, tristique non pharetra in, congue ac sem. Donec odio nulla, lobortis vitae risus in, porttitor pretium mauris. Nullam fringilla tortor eu quam luctus, eget bibendum lectus eleifend. Nam facilisis libero tempor rhoncus consequat.</p>', 'Active', '2014-05-12 11:35:46', '2014-05-12 11:35:46');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `news_title` varchar(255) NOT NULL,
  `news_desc` longtext NOT NULL,
  `status` varchar(7) NOT NULL DEFAULT 'Active',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `news_title`, `news_desc`, `status`, `created`, `modified`) VALUES
(1, 'Lorem ipsum dolor sit amet', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec quis eleifend ligula. Cras porttitor accumsan arcu. Morbi id arcu scelerisque dolor condimentum viverra vel sit amet magna. Donec nunc elit, sodales a neque et, rutrum tincidunt leo. Cras id risus sed est vehicula consequat. Morbi in vehicula nunc, nec sodales magna. Integer convallis viverra massa eget varius. Ut eu scelerisque ante. Duis venenatis quam turpis, tincidunt suscipit velit varius ut. In hac habitasse platea dictumst. Mauris ut elit sed erat hendrerit feugiat a gravida lorem. Nulla ut eleifend sem.</p>', 'Active', '2014-05-12 11:33:38', '2014-05-12 11:34:35'),
(2, 'Sed in leo vel justo commodo facilisis ac gravida', '<p>Sed in leo vel justo commodo facilisis ac gravida risus. Cras arcu lectus, malesuada in tincidunt id, faucibus quis leo. Curabitur tincidunt ac turpis at auctor. Cras rhoncus lorem id augue blandit pulvinar. Sed adipiscing posuere nunc non ornare. Quisque accumsan purus nibh, rhoncus rutrum justo ornare eget. Suspendisse mollis libero nec tempus eleifend. Sed nec lacus sit amet mauris faucibus tempus vel non justo. Donec sit amet metus a nisl congue sagittis. Duis quis turpis elementum, volutpat massa sed, dictum mi. Fusce non tincidunt metus, et facilisis libero. Etiam in interdum sem, non accumsan justo.</p>', 'Active', '2014-05-12 11:33:58', '2014-05-12 11:33:58');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
CREATE TABLE IF NOT EXISTS `pages` (
`id` int(11) NOT NULL AUTO_INCREMENT,
  `model_name` varchar(100) NOT NULL,
  `page_name` varchar(100) NOT NULL,
  `controller_name` varchar(100) NOT NULL,
  `action_name` varchar(100) NOT NULL,
  `icon` varchar(30) NOT NULL,
  `parent_id` int(1) NOT NULL,
  `ordering` int(11) NOT NULL,
  `sel_name` varchar(100) DEFAULT NULL,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `model_name`, `page_name`, `controller_name`, `action_name`, `icon`, `parent_id`, `ordering`, `sel_name`) VALUES
(1, 'Dashboard', 'Dashboard', 'admin', '', 'glyphicon-home', 0, 1, NULL),
(2, 'Subjects', 'Subjects', 'Subjects', 'index', 'glyphicon-th-large', 0, 2, 'Questions,Iequestions'),
(3, 'Subjects', 'Add Question', 'Addquestions', 'add', '', 2, 3, NULL),
(4, 'Students', 'Students', 'Students', 'index', 'glyphicon-user', 0, 4, NULL),
(5, 'Exams', 'Exams', 'Exams', 'index', 'glyphicon-list-alt', 0, 5, 'Attemptedpapers,Addquestions'),
(6, 'Exams', 'Attempted Papers', 'Attemptedpapers', 'index', '', 5, 6, NULL),
(7, 'Results', 'Results', 'Results', 'index', 'glyphicon-asterisk', 0, 7, NULL),
(8, 'Configurations', 'Configurations', 'Configurations', 'index', 'glyphicon-wrench', 0, 8, 'Payments'),
(9, 'Help', 'Help', 'Helps', 'index', 'glyphicon-warning-sign', 0, 11, NULL),
(10, 'Users', 'Users', 'Users', 'index', 'glyphicon-user', 0, 9, NULL),
(11, 'Groups', 'Groups', 'Groups', 'index', 'glyphicon-tasks', 0, 3, NULL),
(12, 'Contents', 'Contents', 'Contents', 'index', 'glyphicon-cog', 0, 10, 'News,Slides,Helpcontents'),
(13, 'Contents', 'Slides', 'Slides', 'index', '', 12, 99, NULL),
(14, 'Contents', 'Organisation Logo', 'Contents', 'index', '', 12, 99, NULL),
(15, 'Contents', 'News Content', 'News', 'index', '', 12, 99, NULL),
(17, 'Contents', 'Help Content', 'Helpcontent', 'index', '', 12, 99, NULL),
(18, 'Questions', 'Questions', 'Questions', 'add', '', 2, 99, NULL),
(19, 'Payments', 'Paypal Payment', 'Payments', 'index', '', 8, 99, NULL);


-- --------------------------------------------------------

--
-- Table structure for table `page_rights`
--

DROP TABLE IF EXISTS `page_rights`;
CREATE TABLE IF NOT EXISTS `page_rights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `ugroup_id` int(11) NOT NULL,
  `save_right` int(1) NOT NULL,
  `update_right` int(1) NOT NULL,
  `view_right` int(1) NOT NULL,
  `search_right` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `page_id` (`page_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `page_rights`
--

INSERT INTO `page_rights` (`id`, `page_id`, `ugroup_id`, `save_right`, `update_right`, `view_right`, `search_right`) VALUES
(1, 1, 2, 0, 0, 1, 0),
(2, 6, 2, 0, 0, 1, 0),
(3, 5, 2, 0, 0, 1, 0),
(4, 11, 2, 0, 0, 1, 0),
(5, 9, 2, 0, 0, 1, 0),
(6, 7, 2, 0, 0, 1, 0),
(7, 4, 2, 0, 0, 1, 0),
(8, 3, 2, 0, 0, 1, 0),
(9, 2, 2, 0, 0, 1, 0),
(10, 18, 2, 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `transaction_id` varchar(20) NOT NULL,
  `amount` decimal(18,2) NOT NULL,
  `remarks` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `paypal_configs`
--

DROP TABLE IF EXISTS `paypal_configs`;
CREATE TABLE IF NOT EXISTS `paypal_configs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(100) NOT NULL,
  `signature` varchar(255) NOT NULL,
  `sandbox_mode` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `paypal_configs`
--

INSERT INTO `paypal_configs` (`id`, `username`, `password`, `signature`, `sandbox_mode`) VALUES
(1, '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `qtypes`
--

DROP TABLE IF EXISTS `qtypes`;
CREATE TABLE IF NOT EXISTS `qtypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_type` varchar(20) NOT NULL,
  `type` char(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `qtypes`
--

INSERT INTO `qtypes` (`id`, `question_type`, `type`) VALUES
(1, 'Objective Questions', 'M'),
(2, 'True / False', 'T'),
(3, 'Fill in the blanks', 'F'),
(4, 'Subjective', 'S');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qtype_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `diff_id` int(11) NOT NULL,
  `question` text NOT NULL,
  `option1` text NOT NULL,
  `option2` text NOT NULL,
  `option3` text NOT NULL,
  `option4` text NOT NULL,
  `option5` text NOT NULL,
  `option6` text NOT NULL,
  `marks` float NOT NULL,
  `negative_marks` float NOT NULL,
  `hint` text NOT NULL,
  `explanation` text NOT NULL,
  `answer` varchar(15) NOT NULL,
  `true_false` varchar(5) NOT NULL,
  `fill_blank` varchar(100) NOT NULL,
  `status` varchar(3) NOT NULL DEFAULT 'Yes',
  PRIMARY KEY (`id`),
  KEY `qtype_id` (`qtype_id`),
  KEY `subject_id` (`subject_id`),
  KEY `diff_id` (`diff_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `qtype_id`, `subject_id`, `diff_id`, `question`, `option1`, `option2`, `option3`, `option4`, `option5`, `option6`, `marks`, `negative_marks`, `hint`, `explanation`, `answer`, `true_false`, `fill_blank`, `status`) VALUES
(1, 1, 2, 2, '<p>What is Force?</p>', '<p>Law</p>', '<p>Mechanical</p>', '<p>State</p>', '<p>None of Above</p>', '', '', 4, 1, '', '', '2', '', '', 'Yes'),
(2, 1, 2, 1, '<p>What is the Unit of Force?</p>', '<p>1</p>', '<p>2</p>', '<p>3</p>', '<p>4</p>', '', '', 4, 0, '', '', '1,4', '', '', 'Yes'),
(3, 4, 5, 3, '<p>Explain Thermal Body</p>', '', '', '', '', '', '', 4, 1, '', '', '', '', '', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

DROP TABLE IF EXISTS `slides`;
CREATE TABLE IF NOT EXISTS `slides` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slide_name` varchar(255) NOT NULL,
  `ordering` int(11) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `dir` varchar(255) NOT NULL,
  `status` varchar(7) NOT NULL DEFAULT 'Active',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`id`, `slide_name`, `ordering`, `photo`, `dir`, `status`, `created`, `modified`) VALUES
(1, 'Slide1', 1, '5ea658b89d2a733da988f6c89b526b22.jpg', '', 'Active', '2014-05-12 11:32:25', '2014-05-12 11:32:25'),
(2, 'Slide2', 2, '796934b99afa95f1a914b4df0cbfed09.jpg', '', 'Active', '2014-05-12 11:32:38', '2014-05-12 11:32:38'),
(3, 'Slide3', 3, '023f182e41f506b0e3fbf736a1b0ea8c.jpg', '', 'Active', '2014-05-12 11:32:51', '2014-05-12 11:32:51');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
CREATE TABLE IF NOT EXISTS `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `guardian_phone` varchar(15) NOT NULL,
  `enroll` varchar(50) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `dir` varchar(255) NOT NULL,
  `status` varchar(7) NOT NULL DEFAULT 'Pending',
  `reg_code` varchar(6) NOT NULL,
  `reg_status` varchar(4) NOT NULL DEFAULT 'Live',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `email`, `password`, `name`, `address`, `phone`, `guardian_phone`, `enroll`, `photo`, `dir`, `status`, `reg_code`, `reg_status`, `created`, `modified`, `last_login`) VALUES
(3, 'student@student.com', 'e41f2b7320732d52cbc55c70a7e96844259d512d9087dde5ff830723b2aa82dc', 'Demo 1', 'Demo 1', '12345679890', '0', 'I-9887457744340', '', '', 'Active', '', 'Done', '2014-04-01 21:40:20', '2014-04-21 15:11:18', '0000-00-00 00:00:00'),
(4, 'student1@student.com', 'e41f2b7320732d52cbc55c70a7e96844259d512d9087dde5ff830723b2aa82dc', 'Demo 2', 'Demo 2', '12345678', '0', '178555', '', '', 'Active', '', 'Done', '2014-04-08 21:38:34', '2014-04-18 16:03:45', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `student_groups`
--

DROP TABLE IF EXISTS `student_groups`;
CREATE TABLE IF NOT EXISTS `student_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`,`group_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=69 ;

--
-- Dumping data for table `student_groups`
--

INSERT INTO `student_groups` (`id`, `student_id`, `group_id`) VALUES
(64, 3, 1),
(65, 3, 2),
(63, 3, 6),
(34, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

DROP TABLE IF EXISTS `subjects`;
CREATE TABLE IF NOT EXISTS `subjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_name` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `subject_name` (`subject_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `subject_name`, `created`, `modified`) VALUES
(1, 'Biology', '2014-03-19 17:40:37', '2014-04-18 16:48:05'),
(2, 'Physics', '2014-03-19 17:41:04', '2014-03-29 13:04:51'),
(3, 'Maths', '2014-03-19 17:40:50', '2014-04-18 16:48:24'),
(5, 'Chemistry', '2014-04-08 14:30:44', '2014-04-08 14:30:44');

-- --------------------------------------------------------

--
-- Table structure for table `ugroups`
--

DROP TABLE IF EXISTS `ugroups`;
CREATE TABLE IF NOT EXISTS `ugroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ugroups`
--

INSERT INTO `ugroups` (`id`, `name`, `created`, `modified`) VALUES
(1, 'Administrator', '2012-07-05 17:16:24', '2012-07-05 17:16:24'),
(2, 'Instructor', '2012-07-05 17:16:34', '2012-07-05 17:16:34');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  `name` varchar(40) DEFAULT NULL,
  `mobile` varchar(10) NOT NULL,
  `ugroup_id` int(11) NOT NULL DEFAULT '2',
  `status` enum('Active','Suspend') DEFAULT 'Active',
  `deleted` char(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `name`, `mobile`, `ugroup_id`, `status`, `deleted`, `created`, `modified`) VALUES
(1, 'admin', 'dfb37faf99ffd691383e054541f1a3fd1966273d359d85aa419562fc26bf4427', 'admin@gmail.com', 'Administrator', '9839512921', 1, 'Active', NULL, '2014-04-01 21:08:06', '2014-04-25 11:50:26');

-- --------------------------------------------------------

--
-- Table structure for table `wallets`
--

DROP TABLE IF EXISTS `wallets`;
CREATE TABLE IF NOT EXISTS `wallets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `in_amount` decimal(18,2) DEFAULT NULL,
  `out_amount` decimal(18,2) DEFAULT NULL,
  `balance` decimal(18,2) NOT NULL,
  `date` datetime NOT NULL,
  `type` varchar(2) NOT NULL,
  `remarks` tinytext NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`),
  KEY `student_id_2` (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `exam_groups`
--
ALTER TABLE `exam_groups`
  ADD CONSTRAINT `exam_groups_ibfk_1` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `exam_groups_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `exam_orders`
--
ALTER TABLE `exam_orders`
  ADD CONSTRAINT `exam_orders_ibfk_2` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `exam_orders_ibfk_3` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `exam_questions`
--
ALTER TABLE `exam_questions`
  ADD CONSTRAINT `exam_questions_ibfk_1` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `exam_questions_ibfk_2` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `exam_results`
--
ALTER TABLE `exam_results`
  ADD CONSTRAINT `exam_results_ibfk_1` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `exam_results_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `exam_stats`
--
ALTER TABLE `exam_stats`
  ADD CONSTRAINT `exam_stats_ibfk_1` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `exam_stats_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `exam_stats_ibfk_3` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`),
  ADD CONSTRAINT `exam_stats_ibfk_4` FOREIGN KEY (`exam_result_id`) REFERENCES `exam_results` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `page_rights`
--
ALTER TABLE `page_rights`
  ADD CONSTRAINT `page_rights_ibfk_1` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `questions_ibfk_2` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `questions_ibfk_3` FOREIGN KEY (`qtype_id`) REFERENCES `qtypes` (`id`),
  ADD CONSTRAINT `questions_ibfk_4` FOREIGN KEY (`diff_id`) REFERENCES `diffs` (`id`);

--
-- Constraints for table `student_groups`
--
ALTER TABLE `student_groups`
  ADD CONSTRAINT `student_groups_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `student_groups_ibfk_3` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wallets`
--
ALTER TABLE `wallets`
  ADD CONSTRAINT `wallets_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

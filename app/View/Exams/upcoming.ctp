<?php echo $this->Session->flash();?>
<div class="row">
	<div class="col-md-2">
		<div class="panel panel-default">
		<div class="panel-heading">
			<div class="widget">
				<h4 class="widget-title">My <span>Exam</span></h4>
			</div>
		</div>
		<div class="list-group">
			<?php echo$this->Html->link("Today's Exam",array('controller'=>'Exams','action'=>'Today'),array('class'=>'list-group-item'));?>
			<?php echo$this->Html->link('Upcoming Exam',array('controller'=>'Exams','action'=>'Upcoming'),array('class'=>'list-group-item active'));?>			
		</div>
		</div>
	</div>
	<div class="col-md-10">
		<div class="panel panel-default">
		<div class="panel-heading">
			<div class="widget">
				<h4 class="widget-title">Upcoming <span>Exam</span></h4>
			</div>
		</div>
			<div class="table-responsive">
				<table class="table table-striped">
					<?php if($upcomingExam){?>
					<tr>
						<th colspan="5">These are the exam(s) that are planned you in near future</th>
					</tr>
					<tr>
					<?php foreach($upcomingExam as $post):$id=$post['Exam']['id'];
					$viewUrl=$this->Html->url(array('controller'=>'Exams','action'=>"view/$id"));?>
					<tr>
						<td><?php echo h($post['Exam']['name']);?></td>
						<td><?php echo$post['Exam']['total_marks'];?> Marks</td>
						<td><?php echo$this->Time->format('d-m-Y',$post['Exam']['start_date']);?></td>
						<?php if($post['Exam']['paid_exam']=="1"){?><td> Amount <?php echo$post['Exam']['amount'];?></td><?php }else{?><td>&nbsp;</td><?php }?>
						<td><?php echo$this->Html->link('View Details','#',array('onclick'=>"show_modal('$viewUrl');"));?></td>
					</tr>
					<?php endforeach;
					unset($post);unset($id);}else{?>
					<tr>
						<th colspan="5">No Exams found for future.</th>
					</tr>
					<?php }?>
				</table>
			</div>
		</div> 
	</div>
</div>
<div class="modal fade" id="targetModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-content">        
  </div>
</div>
<div class="row">
<?php echo $this->Form->create('Exam', array('controller'=>'Exams','action' =>"save/$examId/$oquesNo",'name'=>'post_req','id'=>'post_req'));?>
	<div class="col-md-9">
		<div class="panel panel-default">
			<div class="panel-heading"><strong><?php echo h($post['Exam']['name']);?></strong></div>
				<div class="panel-body">
					<p><small>You have started the exam on <?php echo $this->Time->format('d M, Y [H:i:s]',$examResult['ExamResult']['start_time']);?>.
							Time duration for this exam is <?php echo$this->Function->secondsToWords($post['Exam']['duration']*60);?>.
							Your exam will be submitted automatically <?php echo$this->Time->format('d M, Y [H:i:s]',$this->Time->fromString($examResult['ExamResult']['start_time'])+($post['Exam']['duration']*60));?>.</small></p>
					<div class="panel panel-default">
					<div class="panel-heading"><strong>Question No. <?php echo$userExamQuestion['ExamStat']['ques_no'];?>&nbsp;&nbsp;[ <?php echo$userExamQuestion['ExamStat']['marks'];?> Marks ]&nbsp;&nbsp;<?php echo h($userExamQuestion['Subject']['subject_name']);?> Section</strong></div>
					<div class="table-responsive">
						<table class="table table-striped table">
							<thead>
							<tr>
								<td><?php echo str_replace("<script","",$userExamQuestion['Question']['question']);?></td>
							</tr>
							</thead>
							<tr>
								<td><?php echo (strlen($userExamQuestion['Question']['hint'])>0) ? "<strong>Hint : </strong>".str_replace("<script","",$userExamQuestion['Question']['hint']) : "&nbsp;" ;?></td>
							</tr>
							<?php if($userExamQuestion['Qtype']['type']=="M")
							{
								$options=array();
								if(strlen($userExamQuestion['Question']['option1'])>0)
								$options[1]=str_replace("<script","",$userExamQuestion['Question']['option1']);
								if(strlen($userExamQuestion['Question']['option2'])>0)
								$options[2]=str_replace("<script","",$userExamQuestion['Question']['option2']);
								if(strlen($userExamQuestion['Question']['option3'])>0)
								$options[3]=str_replace("<script","",$userExamQuestion['Question']['option3']);
								if(strlen($userExamQuestion['Question']['option4'])>0)
								$options[4]=str_replace("<script","",$userExamQuestion['Question']['option4']);
								if(strlen($userExamQuestion['Question']['option5'])>0)
								$options[5]=str_replace("<script","",$userExamQuestion['Question']['option5']);
								if(strlen($userExamQuestion['Question']['option6'])>0)
								$options[6]=str_replace("<script","",$userExamQuestion['Question']['option6']);									       
								?>
							<tr>
								<td>								
									<?php if(strlen($userExamQuestion['Question']['answer'])>1)
									{
										?><div class="checkbox"><?php
										$optionSelected=array();
										$optionSelected=explode(",",$userExamQuestion['ExamStat']['option_selected']);
										echo$this->Form->input('option_selected',
																 array('type'=>'select','multiple' => 'checkbox','label'=>false,
																	   'options'=>$options,
																	   'value'=>$optionSelected,
																	   'escape'=>false));
										?></div><?php
									}
									else
									{
										$optionSelected=$userExamQuestion['ExamStat']['option_selected'];
										echo$this->Form->input('option_selected',
																 array('type'=>'radio','label'=>false,'legend'=>false,'div'=>false,
																	   'options'=>$options,
																	   'value'=>$optionSelected,
																	   'before' => '<div class="radio"><label>','separator' => '</label></div><div class="radio"><label>',
																	   'escape'=>false));
									}
									?>
								</td>
							</tr>				
							<?php }?>
							<?php if($userExamQuestion['Qtype']['type']=="T")
							{?>
							<tr>
								<td>
									<?php echo$this->Form->radio('true_false',array('True'=>'True','False'=>'False'),array('value'=>$userExamQuestion['ExamStat']['true_false'],'hiddenField'=>false,'separator'=> '</div><div class="radio-inline">','legend'=>false,'label'=>array('class'=>'radio-inline')));?>
								</td>
							</tr>
							<?php }?>
							<?php if($userExamQuestion['Qtype']['type']=="F")
							{?>
							<tr>
								<td>
									<?php echo$this->Form->input('fill_blank',array('value'=>$userExamQuestion['ExamStat']['fill_blank'],'label'=>false,'autocomplete'=>'off'));?>
								</td>
							</tr>
							<?php }?>
							<?php if($userExamQuestion['Qtype']['type']=="S")
							{?>
							<tr>
								<td>
									<?php echo$this->Form->input('answer',array('type'=>'textarea','value'=>$userExamQuestion['ExamStat']['answer'],'label'=>false,'class'=>'form-control','rows'=>'7'));?>
								</td>
							</tr>
							<?php }?>
						</table>
					</div>					
					<div class="panel-body">
						<div class="row">
							<div class="col-md-2">
							<?php echo$this->Html->link('&larr; Prev',array('action'=>'start',$examId,$pquesNo),array('class'=>'btn btn-default btn-sm btn-block','escape'=>false));?>
							</div>
							<div class="col-md-2">
							<?php echo$this->Html->link('<span class="glyphicon glyphicon-check"></span>&nbsp;Save','#',array('onclick'=>'callUserAnswerSave();','class'=>'btn btn-success btn-sm btn-block','escape'=>false));?>
							</div>
							<div class="col-md-3">
							<?php echo$this->Html->link('<span class="glyphicon glyphicon-log-out"></span>&nbsp;Save & Next','#',array('onclick'=>'callUserAnswerSaveNext();','class'=>'btn btn-success btn-sm btn-block','escape'=>false));?>
							</div>
							<div class="col-md-3">
							<?php echo$this->Html->link('<span class="glyphicon glyphicon-ban-circle"></span>&nbsp;Reset Answer ',array('action'=>'resetAnswer',$examId,$oquesNo),array('class'=>'btn btn-danger btn-sm btn-block','escape'=>false));?>
							</div>
							<div class="col-md-2">
							<?php echo$this->Html->link('Next &rarr;',array('action'=>'start',$examId,$nquesNo),array('class'=>'btn btn-default btn-sm btn-block','escape'=>false));?>
							</div>
						</div>
					</div>
					<p>&nbsp;</p>
					</div>
				</div>
		</div>
	</div>
	<?php echo$this->Form->input('save',array('type'=>'hidden','name'=>'saveNext','value'=>''));?>
	</form>
	<div class="col-md-3">
		<div id="timer"><div id="maincount"></div></div>
	</div>
	<div class="col-md-3">
		<div class="panel-group" id="accordion">
			<?php foreach($userSectionQuestion as $subjectName=>$quesArr):
			$subjectNameId=str_replace(" ","",h($subjectName));
			?>			
			<div class="panel panel-primary">
				<div class="panel-heading">
				<h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#<?php echo$subjectNameId;?>"><?php echo h($subjectName);?></a></h4>
				</div>
				<div id="<?php echo$subjectNameId;?>" class="panel-collapse collapse <?php echo(h($currSubjectName)==h($subjectName)) ? "in" : "";?>">
					<div class="panel-body">
						<div class="row">
							<?php foreach($quesArr as $value):
							if($oquesNo==$value['ExamStat']['ques_no'])
							$btn_type="info";
							elseif($value['ExamStat']['answered']==1)
							$btn_type="success";
							elseif($value['ExamStat']['opened']==1)
							$btn_type="warning";
							else
							$btn_type="default";?>
							<div class="col-md-3 mrg-1"><?php echo$this->Html->link($value['ExamStat']['ques_no'],array('action'=>'start',$examId,$value['ExamStat']['ques_no']),array('class'=>"btn btn-$btn_type btn-block"));?></div>
							<?php endforeach;unset($quesArr);?>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach;unset($i);unset($value);?>					
		</div>
	</div>
	<div class="col-md-3">
		<?php echo $this->Form->postLink('<span class="glyphicon glyphicon-lock"></span>&nbsp;Finish Exam ',array('action' => 'finish', $examId),
										 array('confirm' => 'After submitting the exam, your exam will closed? Press OK to submit the exam or Cancel to return','class'=>'btn btn-danger btn-sm btn-block','escape'=>false));?>
	</div>
	<div class="col-md-3 mrg-1">
		<div class="panel panel-primary">
			<div class="panel-heading">
			<h4 class="panel-title">Legend</h4>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-6 mrg-1"><span class="btn btn-info btn-small">Current Ques</span></div>
					<div class="col-md-5 mrg-1"><span class="btn btn-default btn-small">Not Visited&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></div>			
				</div>
				<div class="row">
					<div class="col-md-6 mrg-1"><span class="btn btn-success btn-small">Answered&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></div>
					<div class="col-md-5 mrg-1"><span class="btn btn-warning btn-small">Not Answered</span></div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $endTime=$this->Time->format('Y,m-1,d,H,i,s',$this->Time->fromString($examResult['ExamResult']['start_time'])+($post['Exam']['duration']*60));
$expiryUrl=$this->Html->url(array('controller'=>'Exams','action'=>"finish/$examId"));
$serverTimeUrl=$this->Html->url(array('controller'=>'ServerTimes'));?>
<script type="text/javascript">
$(document).ready(function(){
liftoffTime=new Date(<?php echo$endTime;?>);
$("#maincount").countdown({until: liftoffTime, format: 'HMS',expiryUrl:"<?php echo$expiryUrl;?>"});
  function serverTime() {
    var time = null; 
    $.ajax({url: "<?php echo$serverTimeUrl;?>", 
        async: false, dataType: 'text', 
        success: function(text) {            
            time = new Date(text);
        }, error: function(http, message, exc) { 
            time = new Date();
    }}); 
    return time; 
}
});
function callUserAnswerSaveNext()
{
	document.post_req.saveNext.value="Yes";
	document.post_req.submit();
}
function callUserAnswerSave()
{
	document.post_req.submit();
}
</script>
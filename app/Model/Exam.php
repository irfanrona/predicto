<?php
class Exam extends AppModel
{
  public function getUserExam($type,$studentId,$currentDateTime,$limit=0)
  {
    $start_date="";
    $end_date="";
    $examLimit="";
    if($type=="today")
    {
      $start_date=array('Exam.start_date <='=>$currentDateTime);
      $end_date=array('Exam.end_date >'=>$currentDateTime);
    }
    if($type=="upcoming")
    {
      $start_date=array('Exam.start_date >'=>$currentDateTime);
    }
    if($limit>0)
    $examLimit=$limit;
    $this->virtualFields= array('total_marks'=>'SUM(Question.marks)');
    $examList=$this->find('all',array(
                                                'fields'=>array('DISTINCT(Exam.id)','name','start_date','total_marks','paid_exam','amount'),
                                                 'joins'=>array(array('table'=>'exam_questions','alias'=>'ExamQuestion','type'=>'Inner',
                                                                      'conditions'=>array('Exam.id=ExamQuestion.Exam_id')),
                                                                array('table'=>'exam_groups','alias'=>'ExamGroup','type'=>'Inner',
                                                                      'conditions'=>array('Exam.id=ExamGroup.exam_id','')),
                                                                array('table'=>'student_groups','alias'=>'StudentGroup','type'=>'Inner',
                                                                      'conditions'=>array('StudentGroup.group_id=ExamGroup.group_id')),
                                                                array('table'=>'questions','alias'=>'Question','type'=>'Inner',
                                                                      'conditions'=>array('ExamQuestion.question_id=Question.id'))),
                                                 'conditions'=>array($start_date,$end_date,"StudentGroup.student_id=$studentId",'Exam.status'=>'Active','Exam.user_id'=>0),
                                                 'order'=>array('Exam.start_date'=>'asc'),
                                                 'group'=>array('Exam.id','ExamGroup.group_id','Exam.id'),
                                                 'limit'=>$examLimit));
    return $examList;
  }
  public function getSubject($id)
  {
    $ExamQuestion=ClassRegistry::init('ExamQuestion');
    $subjectDetail=$ExamQuestion->find('all',array(
                                                             'fields'=>array('DISTINCT(Subject.id)','Subject.subject_name'),
                                                             'joins'=>array(array('table'=>'questions','type'=>'Inner','alias'=>'Question',
                                                                                  'conditions'=>array('Question.id=ExamQuestion.question_id')),
                                                                            array('table'=>'subjects','type'=>'Inner','alias'=>'Subject',
                                                                                  'conditions'=>array('Subject.id=Question.subject_id'))),
                                                             'conditions'=>array('ExamQuestion.exam_id'=>$id),
                                                             'order'=>'Subject.subject_name asc'));
     return$subjectDetail;
  }
  public function checkPost($id,$studentId)
  {
    $ExamGroup=ClassRegistry::init('ExamGroup');
    $checkPost=$ExamGroup->find('count',array(
                                                         'joins'=>array(array('table'=>'student_groups','alias'=>'StudentGroup',
                                                                              'conditions'=>array('ExamGroup.group_id=StudentGroup.group_id')),
                                                                        array('table'=>'exams','alias'=>'Exam',
                                                                              'conditions'=>array('ExamGroup.exam_id=Exam.id'))),
                                                         'conditions'=>array('ExamGroup.exam_id'=>$id,'StudentGroup.student_id'=>$studentId,'Exam.status'=>'Active','Exam.user_id'=>0)));
    return$checkPost;
  }
  public function totalMarks($id)
  {
    $ExamQuestion=ClassRegistry::init('ExamQuestion');
    $ExamQuestion->virtualFields= array('total_marks'=>'SUM(Question.marks)');
    $totalMarks=$ExamQuestion->find('first',array(
                                                 'fields'=>array('total_marks'),
                                                 'joins'=>array(array('table'=>'questions','alias'=>'Question','type'=>'Inner',
                                                                      'conditions'=>array('ExamQuestion.question_id=Question.id'))),
                                                 'conditions'=>array("ExamQuestion.exam_id=$id")));
    return$totalMarks['ExamQuestion']['total_marks'];
  }
  public function userQuestion($id,$ques_random)
  {
    $ExamQuestion=ClassRegistry::init('ExamQuestion');
    $ExamQuestion->virtualFields= array();
    if($ques_random==1)
    {
      $userQuestion=$ExamQuestion->find('all',array('fields'=>array('exam_id','question_id','Question.marks'),
                                                    'joins'=>array(array('table'=>'questions','alias'=>'Question','type'=>'Inner',
                                                                         'conditions'=>array('ExamQuestion.question_id=Question.id')),
                                                                   array('table'=>'subjects','alias'=>'Subject','type'=>'Inner',
                                                                         'conditions'=>array('Question.subject_id=Subject.id'))),
                                                    'conditions'=>array('exam_id'=>$id),
                                                    'order'=>array('Subject.subject_name asc','rand()')));      
    }
    else
    {
      $userQuestion=$ExamQuestion->find('all',array('fields'=>array('exam_id','question_id','Question.marks'),
                                                    'joins'=>array(array('table'=>'questions','alias'=>'Question','type'=>'Inner',
                                                                         'conditions'=>array('ExamQuestion.question_id=Question.id')),
                                                                   array('table'=>'subjects','alias'=>'Subject','type'=>'Inner',
                                                                         'conditions'=>array('Question.subject_id=Subject.id'))),
                                                    'conditions'=>array('exam_id'=>$id),
                                                    'order'=>array('Subject.subject_name asc','Question.id asc')));      
    }
    return$userQuestion;
  }
  public function userExamInsert($id,$ques_random,$studentId,$currentDateTime)
  {
    $ExamResult=ClassRegistry::init('ExamResult');
    $ExamQuestion=ClassRegistry::init('ExamQuestion');
    $ExamStat=ClassRegistry::init('ExamStat');    
    $totalQuestion=$ExamQuestion->find('count',array('conditions'=>array("exam_id=$id")));
    $totalMarks=$this->totalMarks($id);
    $userQuestion=$this->userQuestion($id,$ques_random);
    $ExamResultArr=array("ExamResult"=>array("exam_id"=>$id,"student_id"=>$studentId,"start_time"=>$currentDateTime,"total_question"=>$totalQuestion,"total_marks"=>$totalMarks));
    $ExamResult->create();
    if ($ExamResult->save($ExamResultArr))
    {
        $lastId=$ExamResult->getInsertID();
        foreach($userQuestion as $ques_no=>$examQuestionArr)
        {
          $ques_no++;
          $ExamStat->create();
          $ExamStat->save(array("exam_result_id"=>$lastId,"exam_id"=>$examQuestionArr['ExamQuestion']['exam_id'],"student_id"=>$studentId,"ques_no"=>$ques_no,
                                "question_id"=>$examQuestionArr['ExamQuestion']['question_id'],'marks'=>$examQuestionArr['Question']['marks']));
        }
    }
  }
  public function userExamQuestion($exam_id,$studentId,$quesNo=0)
  {
    $ExamStat=ClassRegistry::init('ExamStat');
    $examQuestionCount=$ExamStat->find('count',array('conditions'=>array('exam_id'=>$exam_id,'student_id'=>$studentId,'ques_no'=>$quesNo,'closed'=>0)));
    if($examQuestionCount==0)
    $quesNo=1;
    $userExamQuestion=$ExamStat->find('first',array('fields'=>array('ExamStat.id','ExamStat.ques_no','ExamStat.option_selected','ExamStat.true_false','ExamStat.fill_blank',
                                                                    'ExamStat.marks',
                                                                    'Exam.negative_marking','ExamStat.answer','Subject.subject_name','Question.id',
                                                                    'Question.question','Question.option1','Question.option1','Question.option2','Question.option3','Question.option4','Question.option5','Question.option6','Question.negative_marks',
                                                                    'Question.answer','Question.true_false','Question.fill_blank','Question.hint','Qtype.type'),
                                                    'joins'=>array(array('table'=>'questions','alias'=>'Question','type'=>'Inner',
                                                                         'conditions'=>array('ExamStat.question_id=Question.id')),
                                                                   array('table'=>'subjects','alias'=>'Subject','type'=>'Inner',
                                                                         'conditions'=>array('Question.subject_id=Subject.id')),
                                                                   array('table'=>'qtypes','alias'=>'Qtype','type'=>'Inner',
                                                                         'conditions'=>array('Qtype.id=Question.qtype_id')),
                                                                   array('table'=>'exams','alias'=>'Exam','type'=>'Inner',
                                                                         'conditions'=>array('Exam.id=ExamStat.exam_id'))),
                                                    'conditions'=>array('exam_id'=>$exam_id,'student_id'=>$studentId,'ques_no'=>$quesNo,'closed'=>0),
                                                    'order'=>array('question_id asc')));    
    return $userExamQuestion;
  }  
  public function userSectionQuestion($exam_id,$studentId)
  {
    $ExamStat=ClassRegistry::init('ExamStat');
    $subjectName=$this->getSubject($exam_id);
    foreach($subjectName as $value)
    {    
      $userSectionQuestion[$value['Subject']['subject_name']]=$ExamStat->find('all',array('fields'=>array('ExamStat.ques_no','ExamStat.opened','ExamStat.answered'),
                                                    'joins'=>array(array('table'=>'questions','alias'=>'Question','type'=>'Inner',
                                                                         'conditions'=>array('ExamStat.question_id=Question.id'))),
                                                    'conditions'=>array('exam_id'=>$exam_id,'student_id'=>$studentId,'Question.subject_id'=>$value['Subject']['id'],'closed'=>0),
                                                    'order'=>array('ExamStat.ques_no asc')));
    }
    return $userSectionQuestion;
  }
  public function userSubject($exam_id,$quesNo,$studentId)
  {
    if($quesNo==0)
    $quesNo=1;
    $ExamStat=ClassRegistry::init('ExamStat');
    $subjectName=$ExamStat->find('first',array('fields'=>array('Subject.subject_name'),
                                                  'joins'=>array(array('table'=>'questions','alias'=>'Question','type'=>'Inner',
                                                                       'conditions'=>array('ExamStat.question_id=Question.id')),
                                                                 array('table'=>'subjects','alias'=>'Subject','type'=>'Inner',
                                                                         'conditions'=>array('Question.subject_id=Subject.id'))),
                                                  'conditions'=>array('exam_id'=>$exam_id,'student_id'=>$studentId,'ques_no'=>$quesNo,'closed'=>0)
                                                  ));
    return$subjectName['Subject']['subject_name'];
  }
  public function userQuestionRead($exam_id,$quesNo,$studentId)
  {
    if($quesNo==0)
    $quesNo=1;
    $usrQues=array('opened'=>1);
    $ExamStat=ClassRegistry::init('ExamStat');
    $ExamStat->updateAll($usrQues,array('exam_id'=>$exam_id,'student_id'=>$studentId,'ques_no'=>$quesNo,'closed'=>0,'opened'=>0));
  }
  public function userSaveAnswer($exam_id,$quesNo,$studentId,$currentDateTime,$valueArr)
  {
    $ExamStat=ClassRegistry::init('ExamStat');
    $userExamQuestion=$this->userExamQuestion($exam_id,$studentId,$quesNo);
    $id=$userExamQuestion['ExamStat']['id'];    
    $usrQues=array("ExamStat"=>array('id'=>$id,'attempt_time'=>$currentDateTime,'answered'=>1));
    $marksObtained=0;$isAnswer=false;
    if(isset($valueArr['Exam']['option_selected']))
    {
      if(is_array($valueArr['Exam']['option_selected']))
      {
        $usrQues['ExamStat']['option_selected']=implode(",",$valueArr['Exam']['option_selected']);
      }
      else
      {
        $usrQues['ExamStat']['option_selected']=$valueArr['Exam']['option_selected'];
      }
      if($usrQues['ExamStat']['option_selected']==$userExamQuestion['Question']['answer'])
      $isAnswer=true;
    }
    if(isset($valueArr['Exam']['true_false']))
    {
        $usrQues['ExamStat']['true_false']=$valueArr['Exam']['true_false'];
        if(strtolower($usrQues['ExamStat']['true_false'])==strtolower($userExamQuestion['Question']['true_false']))
        $isAnswer=true;
    }
    if(isset($valueArr['Exam']['fill_blank']))
    {
        $usrQues['ExamStat']['fill_blank']=$valueArr['Exam']['fill_blank'];
        if(str_replace(" ","",strtolower($usrQues['ExamStat']['fill_blank']))==str_replace(" ","",strtolower($userExamQuestion['Question']['fill_blank'])))
        $isAnswer=true;
    }
    if($isAnswer==true)
    $marksObtained=$userExamQuestion['ExamStat']['marks'];
    else
    {
        if($userExamQuestion['Exam']['negative_marking']=="Yes" && !isset($valueArr['Exam']['answer']))
        {
            if($userExamQuestion['Question']['negative_marks']==0)
            $marksObtained=0;
            else
            $marksObtained=-($userExamQuestion['Question']['negative_marks']);
            
        }
        else
        $marksObtained=0;
    }
    $usrQues['ExamStat']['marks_obtained']=$marksObtained;
    if(isset($valueArr['Exam']['answer']))
    {
        $usrQues['ExamStat']['answer']=$valueArr['Exam']['answer'];        
    }    
    $ExamStat->save($usrQues);
  }
  public function userResetAnswer($exam_id,$quesNo,$studentId)
  {
    $ExamStat=ClassRegistry::init('ExamStat');
    $currRecord=$ExamStat->find('first',array('fields'=>'id',
                                              'conditions'=>array('exam_id'=>$exam_id,'student_id'=>$studentId,'ques_no'=>$quesNo,'closed'=>0)));
    
    $id=$currRecord['ExamStat']['id'];    
    $usrQues=array("ExamStat"=>array('id'=>$id,'attempt_time'=>null,'answered'=>0,'option_selected'=>null,'answer'=>null,'true_false'=>null,
                                     'fill_blank'=>null,'marks_obtained'=>0));
    $ExamStat->save($usrQues);
  }
  public function userExamFinish($exam_id,$studentId,$currentDateTime)
  {
    $ExamResult=ClassRegistry::init('ExamResult');
    $ExamStat=ClassRegistry::init('ExamStat');
    $Exam=ClassRegistry::init('Exam');
    $ExamResultRecord=$ExamResult->find('first',array('fields'=>array('id'),'conditions'=>array('exam_id'=>$exam_id,'student_id'=>$studentId,'end_time'=>null)));
    $id=$ExamResultRecord['ExamResult']['id'];
    $total_answered=$ExamStat->find('count',array('conditions'=>array('exam_result_id'=>$id,'answered'=>1)));
    $userResult=array('id'=>$id,'end_time'=>$currentDateTime,'total_answered'=>$total_answered);
    $ExamResult->save($userResult);
    $ExamStat->updateAll(array('ExamStat.closed'=>1),
                         array('ExamStat.exam_result_id'=>$id));
    
    $ExamRecord=$Exam->find('first',array('fields'=>array('finish_result'),'conditions'=>array('id'=>$exam_id)));
    $finish_result=$ExamRecord['Exam']['finish_result'];
    if($finish_result==1)
    {
      $examResultId=$id;
      $UserAdmin=ClassRegistry::init('User');
      $UserAdminRecord=$UserAdmin->find('first',array('fields'=>array('id'),'conditions'=>array('name'=>'Administrator')));
      $adminId=$UserAdminRecord['User']['id'];
      $post=$ExamResult->find('first',array('joins'=>array(array('table'=>'exams','alias'=>'Exam','type'=>'left',
                                                                         'conditions'=>array('ExamResult.exam_id=Exam.id'))),
                                                    'conditions'=>array('ExamResult.id'=>$examResultId),
                                                    'fields'=>array('ExamResult.total_marks','Exam.passing_percent')));
        $obtainedMarks=$this->obtainedMarks($examResultId);
        $percent=CakeNumber::precision(($obtainedMarks*100)/$post['ExamResult']['total_marks'],2);
        if($percent>=$post['Exam']['passing_percent'])
        $result="Pass";
        else
        $result="Fail";
        $examResultArr=array('id'=>$examResultId,'user_id'=>$adminId,'finalized_time'=>$currentDateTime,'obtained_marks'=>$obtainedMarks,'percent'=>$percent,'result'=>$result);
        $ExamResult->save($examResultArr);
    }
  }
  public function obtainedMarks($id=null)
  {
    $ExamStat=ClassRegistry::init('ExamStat');
    $ExamStat->virtualFields= array('total_marks'=>'SUM(marks_obtained)');
    $ExamStatArr=$ExamStat->find('first',array('fields'=>array('total_marks'),
                                            'conditions'=>array('exam_result_id'=>$id)));
    $obtainedMarks=$ExamStatArr['ExamStat']['total_marks'];
    return$obtainedMarks;
  }
}
?>
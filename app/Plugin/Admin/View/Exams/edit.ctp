<?php echo $this->Html->css('/Admin/css/bootstrap-multiselect');
echo $this->Html->css('/Admin/css/bootstrap-datetimepicker.min');
echo $this->fetch('css');
echo $this->Html->script('/Admin/js/bootstrap-multiselect');
echo $this->Html->script('/Admin/js/moment');
echo $this->Html->script('/Admin/js/bootstrap-datetimepicker.min');
echo $this->fetch('script');?>
<script type="text/javascript">
    $(document).ready(function(){        
        $('#post_req').validationEngine();
        $('.multiselectgrp').multiselect();
         $('#start_date').datetimepicker();
            $('#end_date').datetimepicker();
            $("#start_date").on("dp.change",function (e) {
               $('#end_date').data("DateTimePicker").setMinDate(e.date);
            });
            $("#end_date").on("dp.change",function (e) {
               $('#start_date').data("DateTimePicker").setMaxDate(e.date);
            });
});
</script>
<div class="row">
<?php echo $this->Session->flash();?>
    <div class="col-md-12">    
        <div class="panel panel-default">
             <div class="panel-heading">
		<div class="widget">
		    <h4 class="widget-title">Edit <span>Exams</span></h4>
		</div>
	    </div>
                <div class="panel-body">
                <?php echo $this->Form->create('Exam', array( 'controller' => 'Exam','name'=>'post_req','id'=>'post_req','class'=>'form-horizontal'));?>
                <?php foreach ($Exam as $k=>$post): $id=$post['Exam']['id'];$form_no=$k+1;?>
		<script type="text/javascript">
    $(document).ready(function(){        
         $('#start_date<?php echo$id;?>').datetimepicker();
            $('#end_date<?php echo$id;?>').datetimepicker();
            $("#start_date<?php echo$id;?>").on("dp.change",function (e) {var d= new Date(e.date);
               $('#end_date<?php echo$id;?>').data("DateTimePicker").setMinDate(d.setDate(d.getDate()-1));
            });
            $("#end_date<?php echo$id;?>").on("dp.change",function (e) {var d= new Date(e.date);
               $('#start_date<?php echo$id;?>').data("DateTimePicker").setMaxDate(d.setDate(d.getDate()+0));
            });
});
</script>
                    <div class="panel panel-default">
                        <div class="panel-heading"><small class="text-danger"><strong>Form <?php echo$form_no?></stong></small></div>
                        <div class="panel-body">
							 
							   <div class="form-group">
									<label for="group_name" class="col-sm-2 control-label">Exam Name</label>
									<div class="col-sm-4">
									   <?php echo $this->Form->input('name',array('name'=>"data[Exam][$id][name]",'value'=>$post['Exam']['name'],'label' => false,'class'=>'form-control input-sm validate[required]','placeholder'=>'Exam Name','div'=>false));?>
									</div>                    
									<label for="group_name" class="col-sm-2 control-label">Passing Percent</label>
									<div class="col-sm-4">
									   <?php echo $this->Form->input('passing_percent',array('name'=>"data[Exam][$id][passing_percent]",'value'=>$post['Exam']['passing_percent'],'label' => false,'class'=>'form-control input-sm validate[required,custom[number]','placeholder'=>'Passing Percent','div'=>false));?>
									</div>
								</div>
								<div class="form-group">
									<label for="group_name" class="col-sm-2 control-label">Exam Instruction</label>
									<div class="col-sm-10">
									   <?php echo $this->Tinymce->input('instruction',array('name'=>"data[Exam][$id][instruction]",'id'=>$id,'value'=>$post['Exam']['instruction'],'label' => false,'class'=>'form-control input-sm validate[required]','placeholder'=>'Exam Instruction','div'=>false),array('language'=>'en'),'full');?>
									</div>                        
								</div>
								<div class="form-group">
									<label for="group_name" class="col-sm-2 control-label">Exam Duration (Min.)</label>
									<div class="col-sm-4">
									   <?php echo $this->Form->input('duration',array('name'=>"data[Exam][$id][duration]",'value'=>$post['Exam']['duration'],'label' => false,'class'=>'form-control input-sm validate[required]','placeholder'=>'Exam Duration (Min.)','div'=>false));?>
									</div>                    
									<label for="group_name" class="col-sm-2 control-label">Attempt Count</label>
									<div class="col-sm-4">
									   <?php echo $this->Form->input('attempt_count',array('name'=>"data[Exam][$id][attempt_count]",'value'=>$post['Exam']['attempt_count'],'label' => false,'class'=>'form-control input-sm validate[required,custom[number]','placeholder'=>'Attempt Count','div'=>false));?>
									</div>
								</div>
								<div class="form-group">
									<label for="group_name" class="col-sm-2 control-label">Start Date</label>
									<div class="col-sm-4">
										<div class="input-group date" id="start_date<?php echo$id;?>" data-date-format="YYYY-MM-DD H:m:s">                        
											<?php echo $this->Form->input('start_date',array('name'=>"data[Exam][$id][start_date]",'value'=>$post['Exam']['start_date'],'type'=>'text','readonly'=>'readonly','label' => false,'class'=>'form-control input-sm validate[required]','placeholder'=>'Start Date','div'=>false));?>
											<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
										</div>
									</div>
									<label for="group_name" class="col-sm-2 control-label">End Date</label>
									<div class="col-sm-4">
										<div class="input-group date" id="end_date<?php echo$id;?>" data-date-format="YYYY-MM-DD H:m:s">
										   <?php echo $this->Form->input('end_date',array('name'=>"data[Exam][$id][end_date]",'value'=>$post['Exam']['end_date'],'type'=>'text','id'=>'end_date','readonly'=>'readonly','label' => false,'class'=>'form-control input-sm validate[required]','placeholder'=>'End Date','div'=>false));?>
										   <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
										</div>
									</div>
								</div>	
								<div class="form-group">
									<label for="group_name" class="col-sm-2 control-label">Declare Result</label>
									<div class="col-sm-4">
									   <?php echo $this->Form->input('declare_result',array('type'=>'radio','options'=>array("Yes"=>"Yes","No"=>"No"),'default'=>'Yes','name'=>"data[Exam][$id][declare_result]",'value'=>$post['Exam']['declare_result'],'legend'=>false,'before' => '<label class="radio-inline">','separator' => '</label><label class="radio-inline">','label' => false,'div'=>false));?>
									</div>                    
									<label for="group_name" class="col-sm-2 control-label">Select Group</label>
									<div class="col-sm-4">
									   <?php $gp2=array();
                                                                                        foreach($post['Group'] as $groupName):
                                                                                $gp2[]= $groupName['id'];?>
                                                                                <?php endforeach;unset($groupName);?>
									   <?php echo $this->Form->select('ExamGroup.group_name',$group_id,array('name'=>"data[ExamGroup][$id][group_name]",'value'=>$gp2,'multiple'=>true,'label' => false,'class'=>'form-control multiselectgrp','div'=>false));unset($gp2);?>
									</div>
								</div>
								<div class="form-group">
									<label for="group_name" class="col-sm-2 control-label">Negative Marking</label>
									<div class="col-sm-4">
									   <?php echo $this->Form->input('negative_marking',array('type'=>'radio','options'=>array("Yes"=>"Yes","No"=>"No"),'default'=>'Yes','name'=>"data[Exam][$id][negative_marking]",'value'=>$post['Exam']['negative_marking'],'legend'=>false,'before' => '<label class="radio-inline">','separator' => '</label><label class="radio-inline">','label' => false,'div'=>false));?>									   
									</div>
									<label for="group_name" class="col-sm-2 control-label">Random Question</label>
									<div class="col-sm-4">
									    <?php echo $this->Form->input('ques_random',array('type'=>'radio','options'=>array("1"=>"Yes","0"=>"No"),'default'=>'0','name'=>"data[Exam][$id][ques_random]",'value'=>$post['Exam']['ques_random'],'legend'=>false,'before' => '<label class="radio-inline">','separator' => '</label><label class="radio-inline">','label' => false,'div'=>false));?>									    
									</div>
								</div>
								<div class="form-group">
									<label for="group_name" class="col-sm-2 control-label">Result After Finish</label>
									<div class="col-sm-4">
									   <?php echo $this->Form->input('finish_result',array('type'=>'radio','options'=>array("1"=>"Yes","0"=>"No"),'default'=>'0','name'=>"data[Exam][$id][finish_result]",'value'=>$post['Exam']['finish_result'],'legend'=>false,'before' => '<label class="radio-inline">','separator' => '</label><label class="radio-inline">','label' => false,'div'=>false));?>
									</div>
									<div class="col-sm-4">
									    
									</div>
								</div>
								<?php if($frontExamPaid>0){?>
								<div class="form-group">
									<label for="group_name" class="col-sm-2 control-label">Paid Exam</label>
									<div class="col-sm-4">
									   <?php echo $this->Form->input('paid_exam',array('type'=>'radio','options'=>array("1"=>"Yes",""=>"No"),'default'=>'','name'=>"data[Exam][$id][paid_exam]",'value'=>$post['Exam']['paid_exam'],'legend'=>false,'before' => '<label class="radio-inline">','separator' => '</label><label class="radio-inline">','label' => false,'div'=>false));?>									   
									</div>
									<label for="group_name" class="col-sm-2 control-label">Amount</label>
									<div class="col-sm-4">
									    <?php echo $this->Form->input('amount',array('name'=>"data[Exam][$id][amount]",'value'=>$post['Exam']['amount'],'label' => false,'class'=>'form-control input-sm validate[custom[number],min[1],condRequired[ExamPaidExam1]]','placeholder'=>'Amount','div'=>false));?>
									</div>
								</div>
								<?php }?>
						</div>	
					</div>		
                    <div class="form-group text-left">
                        <div class="col-sm-offset-3 col-sm-10">
                            <?php echo $this->Form->input('id', array('name'=>"data[Exam][$id][id]",'value'=>$post['Exam']['id'],'type' => 'hidden'));?>                            
                        </div>
                    </div>
                    <?php endforeach; ?>
                        <?php unset($post); ?>
                        <div class="form-group text-left">
                        <div class="col-sm-offset-3 col-sm-10">                            
                            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-refresh"></span> Update</button>
                            <button type="button" class="btn btn-danger" onclick="window.location='../index'"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
App::uses('CakeTime', 'Utility');
class DashboardsController extends AppController
{
    public $components = array('HighCharts.HighCharts');
    public $currentDateTime,$studentId;
    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->authenticate();
        $this->currentDateTime=CakeTime::format('Y-m-d H:i:s',CakeTime::convert(time(),$this->siteTimezone));
        $this->studentId=$this->userValue['Student']['id'];
    }
    public function index()
    {
        $this->loadModel('Exam');
        $todayExam=$this->Exam->getUserExam("today",$this->studentId,$this->currentDateTime,3);
        $upcomingExam=$this->Exam->getUserExam("upcoming",$this->studentId,$this->currentDateTime,3);
        $this->set('upcomingExam',$upcomingExam);
        $this->set('todayExam',$todayExam);
        
        $totalExamGiven=$this->Dashboard->find('count',array('conditions'=>array('Dashboard.student_id'=>$this->studentId)));
        $failedExam=$this->Dashboard->find('count',array('conditions'=>array('Dashboard.student_id'=>$this->studentId,'Dashboard.result'=>'Fail')));
        $userTotalAbsent=$this->Dashboard->userTotalAbsent($this->studentId);
        if($userTotalAbsent<0)
        $userTotalAbsent=0;
        $bestScoreArr=$this->Dashboard->userBestExam($this->studentId);
        $bestScore="";
        $bestScoreDate="";
        if(isset($bestScoreArr['Exam']['name']))
        {
            $bestScore=$bestScoreArr['Exam']['name'];
            $bestScoreDate=CakeTime::format('d M,Y [H:i]',$bestScoreArr['ExamResult']['start_time']);
        }       
        $this->set('totalExamGiven',$totalExamGiven);
        $this->set('failedExam',$failedExam);
        $this->set('userTotalAbsent',$userTotalAbsent);
        $this->set('bestScore',$bestScore);
        $this->set('bestScoreDate',$bestScoreDate);
        
        $performanceChartData=array();
        $currentMonth=CakeTime::format('m',CakeTime::convert(time(),$this->siteTimezone));
        for($i=1;$i<=12;$i++)
        {
          if($i>$currentMonth)
          break;
          $examData=$this->Dashboard->performanceCount($this->studentId,$i);
          $performanceChartData[]=(float) $examData;
        }
        $tooltipFormatFunction ="function() { return '<b>'+ this.series.name +'</b><br/>'+ this.x +': '+ this.y +'% Marks';}";
        $chartName = "My Chartdl";
        $mychart = $this->HighCharts->create($chartName,'line');
        $this->HighCharts->setChartParams(
                                          $chartName,
                                          array(
                                                'renderTo'=> "mywrapperdl",  // div to display chart inside
                                                'title'=> 'My Performance',
                                                'titleAlign'=> 'center',
                                                'creditsEnabled'=> FALSE,
                                                'xAxisLabelsEnabled'=> TRUE,
                                                'xAxisCategories'=> array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'),
                                                'yAxisTitleText'=> 'Percentage',
                                                'tooltipEnabled'=> TRUE,
                                                'tooltipFormatter'=> $tooltipFormatFunction,
                                                'enableAutoStep'=> FALSE,
                                                'plotOptionsShowInLegend'=> TRUE,                                              
                                                )
                                          );
        $series = $this->HighCharts->addChartSeries();
        $series->addName('Exam')->addData($performanceChartData);      
        $mychart->addSeries($series);
        $rankpost=$this->Exam->query("SELECT `points`,`student_id`,`exam_given`,`name`,FIND_IN_SET(`points`, (SELECT GROUP_CONCAT(`points` ORDER BY `points` DESC) FROM (SELECT SUM(`obtained_marks`) AS `points` FROM `exam_results` GROUP BY `student_id`) `exam_ranks` )) AS `rank` FROM (SELECT SUM( `obtained_marks` ) AS `points` ,`student_id`,(SELECT COUNT( `id` ) FROM `exam_results` WHERE `student_id` = `ExamResult`.`student_id`) AS `exam_given`, `Student`.`name` FROM `exam_results` AS `ExamResult`INNER JOIN `students` AS `Student` ON `ExamResult`.`student_id` = `Student`.`id` WHERE `finalized_time` IS NOT NULL GROUP BY `student_id`) `Selection` WHERE `student_id`=$this->studentId");
        $totalPoints=0;
        $position=0;
        if(count($rankpost)>0)
        {
            $position=$rankpost[0][0]['rank'];
            $totalPoints=$rankpost[0]['Selection']['points'];
        }
        $this->set('totalPoints',$totalPoints);
        $this->set('position',$position);
    }
}

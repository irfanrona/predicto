<div class="container">
	<div class="row">
		<div class="col-md-12 mrg">
			<div class="panel panel-default">
			<div class="panel-heading">
				<div class="widget-modal">
					<h4 class="widget-modal-title">Exam <span>Details </span>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</h4>
				</div>
			</div>
			<table class="table">
				<tr>
					<td>Exam Name</td>
					<td><?php echo h($post['Exam']['name']);?></td>
					<td>Duration</td>
					<td><?php echo$this->Function->secondsToWords($post['Exam']['duration']*60);?></td>
				</tr>
				<tr>
					<td>Subjects</td>
					<td colspan="3"><?php foreach ($subjectDetail as $k=>$sd):
                          $k++;?>(<?php echo $k?>) <?php echo h($sd['Subject']['subject_name']);?>
                            <?php endforeach;?>                              
                            <?php unset($k);unset($sd);?></td>					
				</tr>
				<tr>
					<td>Start Date</td>
					<td><?php echo$this->Time->format('d-m-Y',$post['Exam']['start_date']);?></td>
					<td>End Date</td>
					<td><?php echo$this->Time->format('d-m-Y',$post['Exam']['end_date']);?></td>
				</tr>
				<tr>
					<td>Passing Percentage</td>
					<td><?php echo$post['Exam']['passing_percent'];?>%</td>
					<td>Negative Marking</td>
					<td><?php echo$post['Exam']['negative_marking'];?></td>
				</tr>
				<tr>
					<td>Total Questions</td>
					<td><?php echo$totalQuestion;?></td>
					<td>Total Marks</td>
					<td><?php echo$totalMarks;?></td>
				</tr>
				<?php if($post['Exam']['paid_exam']==1){?>
				<tr>
					<td>Amount</td>
					<td colspan="3"><?php echo$post['Exam']['amount'];?></td>					
				</tr><?php }?>
			</table>
			</div>
		</div>
	</div>
</div>
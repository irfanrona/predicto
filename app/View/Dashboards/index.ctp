<?php echo $this->Session->flash();?>
<div class="row">
	<div class="col-md-3">
		<div class="panel panel-default">		
		<div class="panel-heading">
			<div class="widget">
				<h4 class="widget-title">My <span>Exam Stats</span></h4>
			</div>
		</div>
		<div class="table-responsive">
			<table class="table">
				<tr>
					<td><strong>Total Exam Given : </strong><strong class="text-danger"><?php echo$totalExamGiven;?></strong></td>					
				</tr>
				<tr>
					<td><strong>Absent Exams : </strong><strong class="text-danger"><?php echo$userTotalAbsent;?></strong></td>					
				</tr>
				<tr>
					<td><strong>Best Score in : </strong><strong class="text-danger"><?php echo h($bestScore);?></strong></td>
				</tr>
				<tr>
					<td><strong>On : </strong><strong class="text-danger"><?php echo$bestScoreDate?></strong></td>
				</tr>
				<tr>
					<td><strong>Failed in : </strong><strong class="text-danger"><?php echo$failedExam;?> Exam</strong></td>
				</tr>
				<tr>
					<td><strong>Total Points : </strong><strong class="text-danger"><?php echo$totalPoints;?></strong></td>
				</tr>
				<tr>
					<td><strong>Your Position : </strong><strong class="text-danger"><?php echo$position;?></strong></td>
				</tr>
			</table>
		</div>
		</div>
	</div>
	<div class="col-md-9">
		<div class="panel panel-default">
		<div class="panel-heading">
			<div class="widget">
				<h4 class="widget-title"><span>Dashboard</span></h4>
			</div>
		</div>
			<div class="table-responsive">
				<table class="table">
					<tr>
						<td>
							<div class="chart">
							<div id="mywrapperdl"></div>
							<?php echo $this->HighCharts->render("My Chartdl");?>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="panel panel-default">
		<div class="panel-heading">
			<div class="widget">
				<h4 class="widget-title">Today's <span>Exam</span></h4>
			</div>
		</div>
			<div class="table-responsive">
				<table class="table table-striped">
					<?php if($todayExam){?>
					<tr>
						<th colspan="6">These are the exam(s) that can be taken right now.</th>
					</tr>
					<tr>						
					</tr>
					<tr>
					<?php foreach($todayExam as $post): $id=$post['Exam']['id'];
					$viewUrl=$this->Html->url(array('controller'=>'Exams','action'=>"view/$id"));
					$instructionUrl=$this->Html->url(array('controller'=>'Exams','action'=>"instruction/$id"));?>
					<tr>
						<td><?php echo h($post['Exam']['name']);?></td>
						<td><?php echo$post['Exam']['total_marks'];?> Marks</td>
						<td><?php echo$this->Time->format('d-m-Y',$post['Exam']['start_date']);?></td>
						<?php if($post['Exam']['paid_exam']=="1"){?><td> Amount <?php echo$post['Exam']['amount'];?></td><?php }else{?><td>&nbsp;</td><?php }?>
						<td><?php echo$this->Html->link('View Details','#',array('onclick'=>"show_modal('$viewUrl');"));?></td>
						<td><?php echo$this->Html->link('Attempt Now','#',array('onclick'=>"show_modal('$instructionUrl');"));?></td>
					</tr>
					<?php endforeach;
					unset($post);unset($id);}else{?>
					<tr>
						<th colspan="6">No Exams found for today.</th>
					</tr>
					<?php }?>
				</table>
			</div>
		</div> 	
		<div class="panel panel-default">
		<div class="panel-heading">
			<div class="widget">
				<h4 class="widget-title">Upcoming <span>Exam</span></h4>
			</div>
		</div>
			<div class="table-responsive">
				<table class="table table-striped">
					<?php if($upcomingExam){?>
					<tr>
						<th colspan="5">These are the exam(s) that are planned you in near future</th>
					</tr>
					<tr>
					<?php foreach($upcomingExam as $post):$id=$post['Exam']['id'];?>
					<tr>
						<td><?php echo h($post['Exam']['name']);?></td>
						<td><?php echo$post['Exam']['total_marks'];?> Marks</td>
						<td><?php echo$this->Time->format('d-m-Y',$post['Exam']['start_date']);?></td>
						<?php if($post['Exam']['paid_exam']=="1"){?><td> Amount <?php echo$post['Exam']['amount'];?></td><?php }else{?><td>&nbsp;</td><?php }?>
						<td><?php echo$this->Html->link('View Details','#',array('onclick'=>"show_modal('Exams/view/$id');"));?></td>
					</tr>
					<?php endforeach;
					unset($post);unset($id);}else{?>
					<tr>
						<th colspan="5">No Exams found for future.</th>
					</tr>
					<?php }?>
				</table>
			</div>
		</div> 
	</div>
</div>
<div class="modal fade" id="targetModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-content">        
  </div>
</div>
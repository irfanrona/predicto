<?php
class Result extends AppModel
{
  public $useTable="exam_results";  
  public $belongsTo=array('Exam'=>array('className'=>'exams',
                                        'order'=>array('Result.start_time'=>'desc')));
  public function userSubject($id)
  {
    $ExamStat=ClassRegistry::init('ExamStat');
    $userSubject=$ExamStat->find('all',array('fields'=>array('DISTINCT(Subject.id)','Subject.subject_name'),
                                'joins'=>array(array('table'=>'questions','alias'=>'Question','type'=>'Inner',
                                                     'conditions'=>array('ExamStat.question_id=Question.id')),
                                               array('table'=>'subjects','alias'=>'Subject','type'=>'Inner',
                                                     'conditions'=>array('Question.subject_id=Subject.id'))),
                                'conditions'=>array('ExamStat.exam_result_id'=>$id)));
    return$userSubject;
  }
  public function userSubjectMarks($id,$subjectId,$sumField)
  {
    $ExamStat=ClassRegistry::init('ExamStat');
    $ExamStat->virtualFields=array('total_marks'=>"SUM(ExamStat.$sumField)");
    $userSubject=$ExamStat->find('first',array('fields'=>array('total_marks'),
                                'joins'=>array(array('table'=>'questions','alias'=>'Question','type'=>'Inner',
                                                     'conditions'=>array('ExamStat.question_id=Question.id')),
                                               array('table'=>'subjects','alias'=>'Subject','type'=>'Inner',
                                                     'conditions'=>array('Question.subject_id=Subject.id'))),
                                'conditions'=>array('ExamStat.exam_result_id'=>$id,'Subject.id'=>$subjectId)));
    $userSubjectMarks=$userSubject['ExamStat']['total_marks'];
    return$userSubjectMarks;
  }
  public function userMarksheet($id)
  {
    $userSubject=$this->userSubject($id);
    $userMarksheet=array();
    $grandTotalMarks=0;$grandObtainedMarks=0;
    foreach($userSubject as $k=>$subjectValue)
    {
        $totalMarks=$this->userSubjectMarks($id,$subjectValue['Subject']['id'],'marks');
        $obtainedMarks=$this->userSubjectMarks($id,$subjectValue['Subject']['id'],'marks_obtained');
        $grandTotalMarks=$grandTotalMarks+$totalMarks;
        $grandObtainedMarks=$grandObtainedMarks+$obtainedMarks;
        $percent=CakeNumber::precision(($obtainedMarks*100)/$totalMarks,2);
        $userMarksheet[$k]['Subject']['name']=$subjectValue['Subject']['subject_name'];
        $userMarksheet[$k]['Subject']['total_marks']=$totalMarks;
        $userMarksheet[$k]['Subject']['obtained_marks']=$obtainedMarks;
        $userMarksheet[$k]['Subject']['percent']=$percent;
    }
    if($grandTotalMarks==0)
    $grandPercent=0;
    else
    $grandPercent=$percent=CakeNumber::precision(($grandObtainedMarks*100)/$grandTotalMarks,2);
    $userMarksheet['total']['Subject']=array('name'=>'Total','total_marks'=>$grandTotalMarks,'obtained_marks'=>$grandObtainedMarks,'percent'=>$grandPercent);
    return$userMarksheet;
  }  
}
?>
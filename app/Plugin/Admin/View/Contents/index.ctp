<?php echo $this->Session->flash();?>
<div class="row">
    <div class="col-md-12">    
        <div class="panel panel-default">
            <div class="panel-heading">
			<div class="widget">
				<h4 class="widget-title"> <span>Contents</span></h4>
			</div>
		</div>
                <div class="panel-body">
                <ul class="nav nav-pills nav-stacked">
                    <li><?php echo$this->Html->link('Organisation Logo',array('controller'=>'Contents','action'=>'weblogo'));?></li>
                    <li><?php echo$this->Html->link('Manage Slides',array('controller'=>'Slides','action'=>'index'));?></li>
                    <li><?php echo$this->Html->link('News Content',array('controller'=>'News','action'=>'index'));?></li>
                    <li><?php echo$this->Html->link('Help Content',array('controller'=>'Helpcontents','action'=>'index'));?></li>
                </ul>                                         
                </div>
            </div>
        </div>
    </div>
</div>
	<div class="row">
		<div class="col-md-12">
		<div class="panel panel-default">
            <div class="panel-heading">
			<div class="widget">
				<h4 class="widget-title">Attempted <span>Papers</span></h4>
			</div>
		</div>            
                <div class="panel-body">
		<?php echo $this->Html->link('<span class="glyphicon glyphicon-arrow-left"></span>&nbsp;Back To Result',array('action'=>'view',$id),array('escape' => false,'class'=>'btn btn-info'));?>
		<div class="panel-group" id="accordion">
							<?php foreach($post as $k=>$ques):?>
								<div class="panel panel-default">
									<div class="panel-heading">
									 <a data-toggle="collapse" href="#collapse<?php echo$ques['ExamStat']['ques_no'];?>">
										  <strong>Question No. <?php echo $ques['ExamStat']['ques_no'];?>&nbsp;(<?php echo$ques['Qtype']['question_type'];?>)&nbsp;[<?php echo h($ques['Subject']['subject_name']);?>]</strong>
										</a>
									</div>
									<div id="collapse<?php echo$ques['ExamStat']['ques_no'];?>" class="collapse<?php echo($k==0)?"in":"";?>">
										<div class="table-responsive">                    
											<table class="table table-bordered">
												
												<tr>
													<td colspan="4"><?php echo str_replace("<script","",$ques['Question']['question']);?></td>                                
												</tr>
												<?php if($ques['Qtype']['type']=="M"){?>
												<?php if(strlen($ques['Question']['option1'])>0){?>
												<tr class="text-left">
													<td><strong class="text-danger">Option (1)</strong></td>
													<td colspan="3"><?php echo str_replace("<script","",$ques['Question']['option1']);?></td>
												</tr>
												<?php }?>
												<?php if(strlen($ques['Question']['option2'])>0){?>
												<tr class="text-left">
												  <td><strong class="text-danger">Option (2</strong>)</td>
												  <td colspan="3"><?php echo str_replace("<script","",$ques['Question']['option2']);?></td>
												</tr>
												<?php }?>
												<?php if(strlen($ques['Question']['option3'])>0){?>												
												<tr class="text-left">
												  <td><strong class="text-danger">Option (3)</strong></td>
												  <td colspan="3"><?php echo str_replace("<script","",$ques['Question']['option3']);?></td>												
												</tr>
												<?php }?>
												<?php if(strlen($ques['Question']['option4'])>0){?>
												<tr class="text-left">
												  <td><strong class="text-danger">Option (4)</strong></td>
												  <td colspan="3"><?php echo str_replace("<script","",$ques['Question']['option4']);?></td>
												</tr>
												<?php }?>
												<?php if(strlen($ques['Question']['option5'])>0){?>
												<tr class="text-left">
												  <td><strong class="text-danger">Option (5)</strong></td>
												  <td colspan="3"><?php echo str_replace("<script","",$ques['Question']['option5']);?></td>
												</tr>
												<?php }?>
												<?php if(strlen($ques['Question']['option6'])>0){?>
												<tr class="text-left">
												  <td><strong class="text-danger">Option (6)</strong></td>
												  <td colspan="3"><?php echo str_replace("<script","",$ques['Question']['option6']);?></td>
												</tr>
												<?php }?>
												<tr>
													<td colspan="2">Actual Answer: <?php echo$ques['Question']['answer'];?></td>
													<td colspan="2">User Selected: <?php echo$ques['ExamStat']['option_selected'];?></td>
												</tr>
												<?php }?>
												<?php if($ques['Qtype']['type']=="T"){?>                            
												<tr>
													<td colspan="2">Actual Answer: <?php echo$ques['Question']['true_false'];?></td>
													<td colspan="2">User Selected: <?php echo$ques['ExamStat']['true_false'];?></td>
												</tr>
												<?php }?>
												<?php if($ques['Qtype']['type']=="F"){?>                            
												<tr>
													<td colspan="2">Actual Answer: <?php echo h($ques['Question']['fill_blank']);?></td>
													<td colspan="2">User Answer: <?php echo h($ques['ExamStat']['fill_blank']);?></td>
												</tr>
												<?php }?>
												<?php if($ques['Qtype']['type']=="S"){?>                            
												<tr>
													<td colspan="4">User Answer</td>
												</tr>
												<tr>
													<td colspan="4"><?php echo h($ques['ExamStat']['answer']);?></td>
												</tr>
												<?php }?>
												<tr>
													<td>Attempted at: <?php echo($ques['ExamStat']['answered']==1)?$this->Time->Format('d-M-Y [H:i:s]',$ques['ExamStat']['attempt_time']):"Not Attempted";?></td>
													<td>Marks: <?php echo$ques['ExamStat']['marks'];?></td>
													<td>Marks Obtained: <?php echo$ques['ExamStat']['marks_obtained'];?></td>
													<?php $userName="";
													if($ques['Qtype']['type']=="S")
													{
														foreach($UserArr as $User):
														if($User['User']['id']==$ques['ExamStat']['user_id'])
														{
															$userName=$User['User']['name'];
															break;
														}
														endforeach;unset($User);
													}?>                                
													<td>Checked by: <?php echo($ques['ExamStat']['user_id']==0)?"System":h($userName);?></td>
												</tr>
												<?php if(strlen($ques['Question']['explanation'])>0){?>
												<tr>
													<td colspan="4"><strong>Explanation</strong></td>
												</tr>
												<tr>
													<td colspan="4"><?php echo str_replace("<script","",$ques['Question']['explanation']);?></td>
												</tr>
												<?php }?>
											</table>
										</div>
									</div>	
								</div>	
							<?php endforeach;unset($ques);?> 
						</div>
		</div>
	</div>
	</div>
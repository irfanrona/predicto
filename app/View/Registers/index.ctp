<?php echo $this->Html->css('/css/bootstrap-multiselect');
echo $this->fetch('css');
echo $this->Html->script('/js/bootstrap-multiselect');
echo $this->fetch('script');?>
<script type="text/javascript">
    $(document).ready(function(){
    $('#post_req').validationEngine();
    $('.multiselectopt').multiselect();
    
});
</script>
<div class="col-md-9">
    <div class="page-heading">
        <div class="widget">
            <h2 class="widget-title">New <span> User Register</span></h2>
        </div>
    </div>
        <?php echo $this->Session->flash();?>
            <?php echo $this->Form->create('Register', array( 'controller' => 'Register', 'action' => 'index','name'=>'post_req','id'=>'post_req','class'=>'form-horizontal','role'=>'form','type' => 'file'));?>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label"><small>Email <span class="text-danger"> *</span></small></label>
                <div class="col-sm-9">
                <?php echo $this->Form->input('email',array('label' => false,'class'=>'form-control validate[required,custom[email]]','placeholder'=>'Email','data-errormessage-value-missing'=>'* What\'s your email address?','data-errormessage-custom-error'=>'* Invalid email address','div'=>false));?>
            </div>
            </div>
            <div class="form-group">
                <label for="group_name" class="col-sm-3 control-label"><small>Student Group<span class="text-danger"> *</span></small></label>
                <div class="col-sm-9">
                <?php echo $this->Form->select('StudentGroup.group_name',$group_id,array('multiple'=>true,'label' => false,'class'=>'form-control multiselectopt validate[required]','placeholder'=>'Student Group','div'=>false));?>
                </div>
            </div>
            <div class="form-group">
                <label for="group_name" class="col-sm-3 control-label"><small>Name<span class="text-danger"> *</span></small></label>
                <div class="col-sm-9">
                <?php echo $this->Form->input('name',array('label' => false,'class'=>'form-control input-sm validate[required]','placeholder'=>'Name','data-errormessage'=>'* What\'s your name?','div'=>false));?>
                </div>
            </div>
            <div class="form-group">
                <label for="group_name" class="col-sm-3 control-label"><small>Password<span class="text-danger"> *</span></small></label>
                <div class="col-sm-9">
                <?php echo $this->Form->input('password',array('label' => false,'class'=>'form-control input-sm validate[required,minSize[4],maxSize[15]]','placeholder'=>'Password','data-errormessage-value-missing'=>'* Password can not be blank','data-errormessage-range-underflow'=>'* Password must be at least 4 characters.','data-errormessage-range-overflow'=>'* Password maximum 15 characters allowed.','div'=>false,'value'=>''));?>
                </div>            </div>
            <div class="form-group">
                <label for="group_name" class="col-sm-3 control-label"><small>Address<span class="text-danger"> *</span></small></label>
                <div class="col-sm-9">
                <?php echo $this->Form->input('address',array('label' => false,'class'=>'form-control input-sm validate[required]','placeholder'=>'Address','data-errormessage'=>'* What\'s your address?','div'=>false));?>
                </div>
            </div>
            <div class="form-group">
                <label for="group_name" class="col-sm-3 control-label"><small>Phone<span class="text-danger"> *</span></small></label>
                <div class="col-sm-9">
                <?php echo $this->Form->input('phone',array('label' => false,'class'=>'form-control input-sm validate[required]','placeholder'=>'Phone','data-errormessage'=>'* What\'s your phone?','div'=>false));?>
                </div>
            </div>
            <div class="form-group">
                <label for="group_name" class="col-sm-3 control-label"><small>Guardian Phone</small></label>
                <div class="col-sm-9">
                <?php echo $this->Form->input('guardian_phone',array('label' => false,'class'=>'form-control input-sm','placeholder'=>'Guardian Phone','div'=>false));?>
                </div>
            </div>
            <div class="form-group">
                <label for="group_name" class="col-sm-3 control-label"><small>Enrolment Number</small></label>
                <div class="col-sm-9">
                <?php echo $this->Form->input('enroll',array('label' => false,'class'=>'form-control input-sm','placeholder'=>'Enrolment Number','div'=>false));?>
                </div>
            </div>
            <div class="form-group">
                <label for="group_name" class="col-sm-3 control-label"><small>Upload Photo</small></label>
                <div class="col-sm-9">
                <?php echo $this->Form->input('photo',array('type' => 'file','label' => false,'class'=>'form-control input-sm','placeholder'=>'Upload Photo','div'=>false));?>
                </div>
            </div>
            <div class="form-group">
                <label for="group_name" class="col-sm-3 control-label"><small>Security Code<span class="text-danger"> *</span></small></label>
                <div class="col-sm-9">
                <?php echo$this->Captcha->render($captchaSettings);?>
                </div>
            </div>            
            <div class="form-group text-center">
                <div class="col-sm-offset-3 col-sm-2">
                <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-log-in"></span> Submit</button>
                </div>
            </div>
            <?php echo$this->Form->end();?>
</div>
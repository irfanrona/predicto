<?php echo $this->Session->flash();?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
			<div class="widget">
				<h4 class="widget-title">Leader <span>Board</span></h4>
			</div>
		</div>
            <div class="table-responsive">
                <table class="table table-striped">
                    <tr>
                        <th>Rating</th>
                        <th>Name</th>
                        <th>Total Point</th>
                        <th>Exam Given</th>
                    </tr>
                    <?php foreach($scoreboard as $post):?>
                    <tr>
                        <td><?php echo$post[0]['rank'];?></td>
                        <td><?php echo h($post['Selection']['name']);?></td>
                        <td><?php echo$post['Selection']['points'];?></td>
                        <td><?php echo$post['Selection']['exam_given'];?></td>
                    </tr>
                    <?php endforeach;unset($post);?>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
<?php echo $this->Session->flash();?>
    <div class="col-md-12">    
        <div class="panel panel-default">
	    <div class="panel-heading">
			<div class="widget">
				<h4 class="widget-title">Edit <span>Help Content</span></h4>
			</div>
		</div>            
                <div class="panel-body">
					<?php echo $this->Form->create('Helpcontent', array( 'controller' => 'Helpcontents','name'=>'post_req','id'=>'post_req','class'=>'form-horizontal'));?>
					<?php foreach ($Helpcontent as $k=>$post): $id=$post['Helpcontent']['id'];$form_no=$k+1;?>
						<div class="panel panel-default">
							<div class="panel-heading"><strong><small class="text-danger">Form <?php echo$form_no?></small></strong></div>
							<div class="panel-body">
								<div class="form-group">
									<label for="group_name" class="col-sm-3 control-label">Title</label>
									<div class="col-sm-9">
									   <?php echo $this->Form->input('link_title',array('name'=>"data[Helpcontent][$id][link_title]",'value'=>$post['Helpcontent']['link_title'],'label' => false,'class'=>'form-control input-sm validate[required]','data-errormessage'=>'Helpcontent Title Required','placeholder'=>'Title','div'=>false));?>
									</div>
								</div>
								<div class="form-group">
									<label for="group_name" class="col-sm-3 control-label">Description</label>
									<div class="col-sm-9">
									   <?php echo $this->Tinymce->input('link_desc',array('name'=>"data[Helpcontent][$id][link_desc]",'id'=>$id,'value'=>$post['Helpcontent']['link_desc'],'label' => false,'class'=>'form-control input-sm validate[required]','placeholder'=>'Description','div'=>false),array('language'=>'en'),'full');?>
									</div>                        
								</div>
								<div class="form-group">
								    <label for="group_name" class="col-sm-3 control-label">Status</label>
								    <div class="col-sm-9">
									<?php echo $this->Form->select('status',array("Active"=>"Active","Suspend"=>"Suspend"),array('name'=>"data[Helpcontent][$id][status]",'value'=>$post['Helpcontent']['status'],'empty'=>null,'label' => false,'class'=>'form-control input-sm validate[required]','div'=>false));?>
								    </div>
								</div>
								<div class="form-group text-left">
									<div class="col-sm-offset-3 col-sm-10">
										<?php echo $this->Form->input('id', array('name'=>"data[Helpcontent][$id][id]",'value'=>$post['Helpcontent']['id'],'type' => 'hidden'));?>                            
									</div>
								</div>
							</div>	
						</div>						
                    <?php endforeach; ?>
                        <?php unset($post); ?>
                        <div class="form-group text-left">
                        <div class="col-sm-offset-3 col-sm-10">                            
                            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-refresh"></span> Update</button>
                            <button type="button" class="btn btn-danger" onclick="window.location='../index'"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
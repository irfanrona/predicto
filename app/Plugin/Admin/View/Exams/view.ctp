<div class="container">
<div class="row">
<?php echo $this->Session->flash();
?>
    <div class="col-md-12">    
        <div class="panel panel-default mrg">
	    <div class="panel-heading"><div class="widget-modal"><h4 class="widget-modal-title">Exam <span>Details</span><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button></strong></h4></div></div>            
				<div class="panel-body">
                    <div class="table-responsive"> 
						<table class="table table-bordered">
							<tr>
								<td><strong><small class="text-danger">Exam Name</small></strong></td>
								<td><?php echo h($post['Exam']['name']);?></td>
								<td><?php if($post['Exam']['status']=="Inactive"){echo$this->Html->link('Add Questions',array('controller'=>'Addquestions','action'=>"index/$id"));}
                                                                if($post['Exam']['status']=="Active"){echo$this->Html->link('Finalize Result',array('controller'=>'Attemptedpapers','action'=>"index/$id"));}
                                                                if($post['Exam']['status']=="Closed"){echo$this->Html->link('Attempted Papers',array('controller'=>'Attemptedpapers','action'=>"index/$id"));}?></td>
							</tr>
							<tr>
								<td><strong><small class="text-danger">Eligible Group</small></strong></td>
								<td><?php foreach($post['Group'] as $k=>$groupName):?>
                                                                                (<?php echo++$k;?>) <?php echo h($groupName['group_name']);?>
                                                                                <?php endforeach;unset($groupName);unset($k);?>
								</td>
								<td><?php if($post['Exam']['status']=="Active"){echo$this->Form->postLink('Close Exam',array('controller'=>'Attemptedpapers','action'=>'closeexam',$id),array('confirm'=>'Are you sure, you want to close this exam?'));}
                                                                if($post['Exam']['status']=="Inactive"){echo$this->Form->postLink('Activate Exam',array('action'=>'activateexam',$id),array('confirm'=>'Are you sure, you want to activate this exam?'));}
                                                                if($post['Exam']['status']=="Closed"){?>Exam Closed<?php }
                                                                ?></td>
							</tr>
							<tr>
								<td><strong><small class="text-danger">Start Date</small></strong></td>
								<td colspan="2"><?php echo $this->Time->format('d F Y',$post['Exam']['start_date']);?></td>
							</tr>
							<tr>
								<td><strong><small class="text-danger">End Date</small></strong></td>
								<td colspan="2"><?php echo $this->Time->format('d F Y',$post['Exam']['end_date']);?></td>
							</tr>
							<tr>
								<td><strong><small class="text-danger">Total Question</small></strong></td>
								<td colspan="2"><?php echo $TotalQuestion;?></td>
							</tr>
						</table>
					</div>	
					<div class="table-responsive"> 	
						<table class="table table-bordered">
							<tr class="text-danger">
								<th><small>Subject</small></th>
								<th><small>Subjective</small></th>
								<th><small>Objective</small></th>
								<th><small>True &amp; False</small></th>
								<th><small>Fill in the blanks</small></th>
								<th><small>Difficulty Level</small></th>
							</tr>                    
							<?php foreach($SubjectDetail as $sd):?>
							<tr><td><?php echo$subject_name= h($sd['Subject']['subject_name']);?></td>
								<?php for($i=0;$i<4;$i++):?>
								<td><?php echo$QuestionDetail[$subject_name][$i];?></td>
								<?php endfor;?>
								<td>
								<?php $i=0; foreach($DiffLevel as $diff):?>
								<?php echo$DifficultyDetail[$subject_name][$i]."(".$diff['Diff']['type'].")";?>
								<?php $i++;endforeach;?></td>
							</tr>
							<?php endforeach;?>
							<?php unset($sd);?>
                        </table>
					</div>	
					<div class="table-responsive"> 	
						<table class="table table-bordered">
							<tr>
								<?php $i=0;foreach($SubjectDetail as $sd):?>
								<td>
									<div class="chart">	
										<div id="piewrapper<?php echo$i?>"></div>
										<?php echo $this->HighCharts->render("Pie Chart$i"); ?>
									</div>
								</td>
								<?php $i++;endforeach;?>
							</tr>
						</table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

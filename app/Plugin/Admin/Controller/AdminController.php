<?php
App::uses('CakeTime', 'Utility');
App::uses('CakeNumber', 'Utility');
class AdminController extends AdminAppController
{
    public $components = array('HighCharts.HighCharts');
    public function index()
    {
        // Upcoming Exam Statistic
        $this->loadModel('Exam');
        $this->Exam->bindModel(array('hasAndBelongsToMany'=>array('Group'=>array('className'=>'Group',
                                                     'joinTable' => 'exam_groups',
                                                     'foreignKey' => 'exam_id',
                                                     'associationForeignKey' => 'group_id'))));        
        $currentDateTime=CakeTime::format('Y-m-d H:i:s',CakeTime::convert(time(),$this->siteTimezone));
        $this->Exam->virtualFields= array('total_marks' => 'SUM(Question.marks)');
        $UpcomingExam=$this->Exam->find('all',array(
                                                 'fields'=>array('Exam.id','name','start_date','duration','total_marks'),
                                                 'joins'=>array(array('table'=>'exam_questions','alias'=>'ExamQuestion','type'=>'Inner',
                                                                      'conditions'=>array('Exam.id=ExamQuestion.Exam_id')),
                                                                array('table'=>'questions','alias'=>'Question','type'=>'Inner',
                                                                      'conditions'=>array('ExamQuestion.question_id=Question.id'))),
                                                 'conditions'=>array('Exam.start_date >'=>$currentDateTime,'Exam.status'=>'Active'),
                                                 'order'=>array('Exam.start_date'=>'asc'),
                                                 'group'=>array('Exam.id'),
                                                 'limit'=>3));
        $total_inprogress_exam=$this->Exam->find('count',array('conditions'=>array('Exam.start_date <='=>$currentDateTime,'Exam.end_date >'=>$currentDateTime,'Exam.status'=>'Active')));
        $total_upcoming_exam=$this->Exam->find('count',array('conditions'=>array('Exam.start_date >'=>$currentDateTime,'Exam.status'=>'Active')));
        $total_completed_exam=$this->Exam->find('count',array('conditions'=>array('Exam.status'=>'Closed')));
        $this->set('total_inprogress_exam',$total_inprogress_exam);
        $this->set('total_upcoming_exam',$total_upcoming_exam);
        $this->set('total_completed_exam',$total_completed_exam);
        $this->set('UpcomingExam',$UpcomingExam); 
        // End Exam Statistic
        
        // Start Student Statistic
        $studentStatitics=$this->Admin->studentStatitics();
        $groupxAxis=array();
        $activeData=array();$pendingData=array();$suspendData=array();
        foreach($studentStatitics as $studentValue)
        {
            $groupxAxis[]=$studentValue['GroupName']['name'];
            $activeData[]=$studentValue['GroupName']['active'];
            $pendingData[]=$studentValue['GroupName']['pending'];
            $suspendData[]=$studentValue['GroupName']['suspend'];
        }
        $this->set('studentStatitics',$studentStatitics);
        $chartName = "My Chartd2";
        $mychart = $this->HighCharts->create($chartName,'column');
        $this->HighCharts->setChartParams(
                                          $chartName,
                                          array(
                                                'renderTo'=> "mywrapperd2",  // div to display chart inside
                                                'title'=> 'User Details',
                                                'xAxisLabelsEnabled'=> TRUE,
                                                'xAxisCategories'=> $groupxAxis,
                                                'yAxisTitleText'=> '',
                                                'enableAutoStep'=> FALSE,
                                                'creditsEnabled'=> FALSE,
                                                'legendEnabled'=> TRUE,                                                
                                                )
                                          );
        $activeSeries = $this->HighCharts->addChartSeries();
        $pendingSeries = $this->HighCharts->addChartSeries();
        $suspendSeries  = $this->HighCharts->addChartSeries();        
        $activeSeries->addName('Active')->addData($activeData);
        $pendingSeries->addName('Pending')->addData($pendingData);
        $suspendSeries->addName('Suspend')->addData($suspendData);        
        $mychart->addSeries($activeSeries);
        $mychart->addSeries($pendingSeries);
        $mychart->addSeries($suspendSeries);
        // End Student Statistic
        
        // Start Recent Exam Result Result
        $recentExamResult=$this->Admin->recentExamResult();
        foreach($recentExamResult as $recentValue)
        {
            $chartRerData=array();
            $chartRerData[]=array('Pass',$recentValue['RecentExam']['StudentStat']['pass']);
            $chartRerData[]=array('Fail',$recentValue['RecentExam']['StudentStat']['fail']);
            $chartRerData[]=array('Absent',$recentValue['RecentExam']['StudentStat']['absent']);
            $id=$recentValue['RecentExam']['Exam']['id'];
            $chartName = "My Chartss$id";
            $mychart = $this->HighCharts->create($chartName,'pie');
            $this->HighCharts->setChartParams(
                                              $chartName,
                                              array(
                                                    'renderTo'=> "mywrapperss$id",  // div to display chart inside                                                
                                                    'creditsEnabled'=> FALSE,
                                                    'chartWidth'=> 300,
                                                    'chartHeight'=> 200,
                                                    'plotOptionsPieShowInLegend'=> TRUE,
                                                    'plotOptionsPieDataLabelsEnabled'=> TRUE,
                                                    'plotOptionsPieDataLabelsFormat'=>'{point.name}:<b>{point.percentage:.1f}%</b>',
                                                    )
                                              );
            
            $series = $this->HighCharts->addChartSeries();
            $series->addName('Student')->addData($chartRerData);
            $mychart->addSeries($series);
            
            $chartRerData=array();$chartRerData1=array();
            $chartRerData=array($recentValue['RecentExam']['OverallResult']['passing']);
            $chartRerData1=array($recentValue['RecentExam']['OverallResult']['average']);
            $id=$recentValue['RecentExam']['Exam']['id'];
            $chartName = "My Chartor$id";
            $mychart = $this->HighCharts->create($chartName,'bar');
            $this->HighCharts->setChartParams(
                                              $chartName,
                                              array(
                                                    'renderTo'=> "mywrapperor$id",  // div to display chart inside                                                
                                                    'creditsEnabled'=> FALSE,
                                                    'chartWidth'=> 350,
                                                    'chartHeight'=> 200,
                                                    'legendEnabled'=> TRUE,                                                    
                                                    'plotOptionsBarDataLabelsEnabled'=> TRUE,                                                    
                                                    )
                                              );
            
            $series = $this->HighCharts->addChartSeries();
            $series1 = $this->HighCharts->addChartSeries();
            $series->addName('Passing %age')->addData($chartRerData);
            $series1->addName('Average %age')->addData($chartRerData1);
            $mychart->addSeries($series);
            $mychart->addSeries($series1);
        }        
        $this->set('recentExamResult',$recentExamResult);
        // End Recent Exam Result Result
        
        // Start Question Bank
        $this->loadModel('Subject');
        $this->loadModel('Diff');
        $Subject=$this->Subject->find('all');
        $DiffLevel=$this->Diff->find('all');
        $chartData=array();$subjectxAxis=array();
        $easyData=array();$mediumData=array();$difficultData=array();
        $DifficultyDetail=array();
        foreach($Subject as $value)
        {
            $subjectId=$value['Subject']['id'];
            $subjectName=$value['Subject']['subject_name'];
            $easy=$this->Admin->viewdifftype($subjectId,'E');
            $medium=$this->Admin->viewdifftype($subjectId,'M');
            $difficult=$this->Admin->viewdifftype($subjectId,'D');
            $DifficultyDetail[$subjectName][]=$easy;
            $DifficultyDetail[$subjectName][]=$medium;
            $DifficultyDetail[$subjectName][]=$difficult;
            $totalQuestion=$easy+$medium+$difficult;
            $DifficultyDetail[$subjectName]['total_question']=$totalQuestion;
            $chartData[]=array($subjectName,$totalQuestion);
            $subjectxAxis[]=$subjectName;
            $j=0;
            $easyData[]=$easy;
            $mediumData[]=$medium;
            $difficultData[]=$difficult;            
        }
        $chartName = "Pie Chartqc";
        $pieChart = $this->HighCharts->create($chartName,'pie');
        $this->HighCharts->setChartParams(
                                          $chartName,
                                          array(
                                                'renderTo'=> "piewrapperqc",  // div to display chart inside
                                                
                                                'title'=> 'Question Count',
                                                'titleAlign'=> 'center',
                                                'creditsEnabled'=> FALSE,
                                                'plotOptionsShowInLegend'=> TRUE,
                                                'plotOptionsPieShowInLegend'=> TRUE,
                                                'plotOptionsPieDataLabelsEnabled'=> TRUE,
                                                'plotOptionsPieDataLabelsFormat'=>'{point.name}:<b>{point.y}</b>',
                                                )
                                          );
        
        $series = $this->HighCharts->addChartSeries();
        $series->addName('Total Question')->addData($chartData);
        $pieChart->addSeries($series);
        
        $chartName = "My Chartdl";
        $mychart = $this->HighCharts->create($chartName,'column');
        $this->HighCharts->setChartParams(
                                          $chartName,
                                          array(
                                                'renderTo'=> "mywrapperdl",  // div to display chart inside
                                                
                                                'title'=> 'Question Bank Difficulty Wise',
                                                'xAxisLabelsEnabled'=> TRUE,
                                                'xAxisCategories'=> $subjectxAxis,
                                                'yAxisTitleText'=> '',
                                                'enableAutoStep'=> FALSE,
                                                'creditsEnabled'=> FALSE,
                                                'plotOptionsSeriesStacking'=> 'percent',
                                                'legendEnabled'=> TRUE,
                                                'legendLayout'=> 'vertical',
                                                'legendVerticalAlign'=> 'middle',
                                                'legendAlign'=> 'right',
                                                'plotOptionsColumnDataLabelsEnabled'=> TRUE,
                                                'plotOptionsColumnDataLabelsColor'=> 'white',                                                
                                                )
                                          );
        $easySeries = $this->HighCharts->addChartSeries();
        $mediumSeries = $this->HighCharts->addChartSeries();
        $difficultSeries  = $this->HighCharts->addChartSeries();
        
        $easySeries->addName('Easy')->addData($easyData);
        $mediumSeries->addName('Normal')->addData($mediumData);
        $difficultSeries->addName('Difficult')->addData($difficultData);
        
        $mychart->addSeries($easySeries);
        $mychart->addSeries($mediumSeries);
        $mychart->addSeries($difficultSeries);
        $this->set('Subject',$Subject);
        $this->set('DifficultyDetail',$DifficultyDetail);
        $this->set('DiffLevel',$DiffLevel);
        // End Question Bank
               
    }    
}
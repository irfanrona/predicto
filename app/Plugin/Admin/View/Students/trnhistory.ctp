<div class="row">
<?php echo $this->element('pagination',array('IsAction'=>'trnhistory'));
        $page_params = $this->Paginator->params();
        $limit = $page_params['limit'];
        $page = $page_params['page'];	
        $serial_no = 1*$limit*($page-1)+1;?>
    <div class="col-md-12">
    <div class="btn-group">
        <?php echo $this->Html->link('<span class="glyphicon glyphicon-arrow-left"></span>&nbsp;Back To Student',array('controller' => 'Students','action'=>'index'),array('escape' => false,'class'=>'btn btn-info'));?>
    </div>
	<div class="panel panel-default">
	    <div class="panel-heading">
		<div class="widget">
		    <h4 class="widget-title">Transaction <span>History</span></h4>
		</div>
	    </div>	    
	    <div class="table-responsive">
		<table class="table table-striped">
		    <tr>
			<th><?php echo $this->Paginator->sort('id', 'S.No.', array('direction' => 'desc'));?></th>
			<th><?php echo $this->Paginator->sort('email', 'Email', array('direction' => 'asc'));?></th>
			<th><?php echo $this->Paginator->sort('in_amount', 'Credit', array('direction' => 'asc'));?></th>
			<th><?php echo $this->Paginator->sort('out_amount', 'Debit', array('direction' => 'asc'));?></th>
			<th><?php echo $this->Paginator->sort('balance', 'Balance', array('direction' => 'asc'));?></th>
			<th><?php echo $this->Paginator->sort('date', 'Date & Time', array('direction' => 'asc'));?></th>
			<th><?php echo $this->Paginator->sort('type', 'Payment Through', array('direction' => 'asc'));?></th>
			<th><?php echo $this->Paginator->sort('remarks', 'Remarks', array('direction' => 'asc'));?></th>
		    </tr>
		    <?php foreach($Transactionhistory as $post):?>
		    <tr>
			<td><?php echo$serial_no++;?></td>
			<td><?php echo h($post['Student']['email']);?></td>
			<td><?php echo$post['Wallet']['in_amount'];?></td>
			<td><?php echo$post['Wallet']['out_amount'];?></td>
			<td><?php echo$post['Wallet']['balance'];?></td>
			<td><?php echo$this->Time->format('d M, Y [H:i]',$post['Wallet']['date']);?></td>
			<td><?php echo$payment_type_arr[$post['Wallet']['type']];?></td>
			<td><?php echo h($post['Wallet']['remarks']);?></td>
		    </tr>
		    <?php endforeach;unset($post);?>
		</table>
	    </div>
	</div>
    </div>
</div>
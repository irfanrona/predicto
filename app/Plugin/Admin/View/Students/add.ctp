<?php echo $this->Html->css('/Admin/css/bootstrap-multiselect');
echo $this->fetch('css');
echo $this->Html->script('/Admin/js/bootstrap-multiselect');
echo $this->fetch('script');?>
<script type="text/javascript">
    $(document).ready(function(){
    $('#post_req').validationEngine();
    $('.multiselect').multiselect();
});
</script>
<?php if(!$isError){?>
<div class="container"><?php }?>
<div class="row">
<?php echo $this->Session->flash();?>
    <div class="col-md-12">    
        <div class="panel panel-default mrg">
            <div class="panel-heading"><div class="widget-modal"><h4 class="widget-modal-title">Add <span>Students</span> <?php if(!$isError){?><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button></strong><?php }?></h4></div></div>
                <div class="panel-body">
                <?php echo $this->Form->create('Student', array( 'controller' => 'Student', 'action' => 'add','name'=>'post_req','id'=>'post_req','type' => 'file'));?>
                    <div class="col-md-6">
						 <div class="form-group">
							<label for="email"><small>Email<span class="text-danger"> *</span></small></label>
							<?php echo $this->Form->input('email',array('label' => false,'class'=>'form-control input-sm validate[required,custom[email]]','placeholder'=>'Email','div'=>false));?>
						</div>
					</div>
                    <div class="col-md-6">
						<div class="form-group">
							<label for="group_name"><small>Student Group<span class="text-danger"> *</span></small></label><br/>
							<?php echo $this->Form->select('StudentGroup.group_name',$group_id,array('multiple'=>true,'label' => false,'class'=>'form-control multiselect validate[required]','placeholder'=>'Student Group','div'=>false));?>
						</div>
					</div>
                    <div class="col-md-6">
                        <div class="form-group">
							<label for="group_name"><small>Name<span class="text-danger"> *</span></small></label>
							<?php echo $this->Form->input('name',array('label' => false,'class'=>'form-control input-sm validate[required]','placeholder'=>'Name','div'=>false));?>
						</div>	
					</div>
                    <div class="col-md-6">
						<div class="form-group">
							<label for="group_name"><small>Password<span class="text-danger"> *</span></small></label>
							<?php echo $this->Form->input('password',array('label' => false,'class'=>'form-control input-sm validate[required,minSize[4],maxSize[15]]','placeholder'=>'Password','div'=>false));?>
						</div>	
                    </div>
                    <div class="col-md-12">
						<div class="form-group">
							<label for="group_name"><small>Address<span class="text-danger"> *</span></small></label>
							<?php echo $this->Form->input('address',array('label' => false,'class'=>'form-control input-sm validate[required]','placeholder'=>'Address','div'=>false));?>
						</div>	
					</div>
                    <div class="col-md-6">
						<div class="form-group">
							<label for="group_name"><small>Phone<span class="text-danger"> *</span></small></label>
							<?php echo $this->Form->input('phone',array('label' => false,'class'=>'form-control input-sm validate[required]','placeholder'=>'Phone','div'=>false));?>
						</div>	
                    </div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="group_name"><small>Guardian Phone</small></label>
							<?php echo $this->Form->input('guardian_phone',array('label' => false,'class'=>'form-control input-sm','placeholder'=>'Guardian Phone','div'=>false));?>
						</div>	
					</div>
                    <div class="col-md-6">
						<div class="form-group">
							<label for="group_name"><small>Enrolment Number</small></label>
							<?php echo $this->Form->input('enroll',array('label' => false,'class'=>'form-control input-sm','placeholder'=>'Enrolment Number','div'=>false));?>
						</div>
                    </div>
					 <div class="col-md-6">			
						<div class="form-group">
							<label for="group_name"><small>Status</small></label>
							<?php echo $this->Form->select('status',array("Active"=>"Active","Pending"=>"Pending","Suspend"=>"Suspend"),array('empty'=>null,'label' => false,'class'=>'form-control input-sm validate[required]','placeholder'=>'Status','div'=>false));?>
						</div>
					</div>
					 <div class="col-md-6">
						<div class="form-group">
							<label for="group_name"><small>Upload Photo</small></label>
							<?php echo $this->Form->input('photo',array('type' => 'file','label' => false,'class'=>'form-control input-sm','placeholder'=>'Upload Photo','div'=>false));?>
						</div>
                    </div>
                    <div class="form-group text-left">
                        <div class="col-sm-offset-3 col-sm-10">
                            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-plus-sign"></span> Save</button>
                            <?php if(!$isError){?><button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Close</button><?php }?>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if(!$isError){?>
</div><?php }?>
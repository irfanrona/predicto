<?php
class AddquestionsController extends AdminAppController {
    public $helpers = array('Html', 'Form','Session','Paginator');
    public $components = array('Session','Paginator','search-master.Prg');
    public $presetVars = true;
    var $paginate = array('limit'=>20,'maxLimit'=>500,'page'=>1,'order'=>array('Addquestion.id'=>'desc'));
    public function index($id = null)
    {
        $this->Prg->commonProcess();
        $this->Paginator->settings = $this->paginate;
        $this->Paginator->settings['conditions'] = $this->Addquestion->parseCriteria($this->Prg->parsedParams());
        $this->set('Addquestion', $this->Paginator->paginate());       
        $this->loadModel('ExamQuestion');
        $this->loadModel('Subject');
        $this->loadModel('Qtype');
        $this->loadModel('Diff');
        $ExamQuestion=$this->ExamQuestion->findAllByExamId($id,array(),array('ExamQuestion.question_id' => 'desc'));
        
        if($this->request->data['Addquestion'])
        $selectedSubject=$this->request->data['Addquestion']['subject_id'];
        else
        $selectedSubject=null;
        $this->set('subjectId', $this->Subject->find('list',array('fields'=>array('id','subject_name'))));
        $this->set('qtypeId', $this->Qtype->find('list',array('fields'=>array('id','question_type'))));
        $this->set('diffId', $this->Diff->find('list',array('fields'=>array('id','diff_level'))));
        
        $this->set('ExamQuestion',$ExamQuestion);
        $this->set('exam_id',$id);        
    }
    public function adddelete()
    {
        if ($this->request->is('post'))
        {
            $exam_id=$this->request->data['Addquestion']['exam_id'];
            if ($this->data['action']=="add")        
            {
                if ($this->request->is('post'))
                {
                    $this->loadModel('ExamQuestion');
                    foreach($this->data['Addquestion']['id'] as $key => $value)
                    {
                        if($value>0)
                        {
                            $this->ExamQuestion->create();
                            $this->request->data['ExamQuestion']['exam_id']=$exam_id;
                            $this->request->data['ExamQuestion']['question_id']=$value;
                            $eq=$this->ExamQuestion->findByQuestionIdAndExamId($value,$exam_id);
                            if($eq)
                            {
                                $eq_id=$eq['ExamQuestion']['id'];
                                $this->ExamQuestion->delete($eq_id);
                            }
                            $this->ExamQuestion->save($this->request->data['ExamQuestion']);
                        }
                    }
                    $this->Session->setFlash('Your Question has been added for exam.','flash',array('alert'=>'success'));
                }            
            }        
            if ($this->data['action']=="delete")      
            {
                if ($this->request->is('post'))
                {
                    $this->loadModel('ExamQuestion');
                    foreach($this->data['Addquestion']['id'] as $key => $value)
                    {
                        if($value>0)
                        {
                            $eq=$this->ExamQuestion->findByQuestionIdAndExamId($value,$exam_id);                            
                            if($eq)
                            {
                                $eq_id=$eq['ExamQuestion']['id'];
                                $this->ExamQuestion->delete($eq_id);
                            }
                        }
                    }
                    $this->Session->setFlash('Your Question has been deleted for exam.','flash',array('alert'=>'danger'));
                }                    
            }
        }
        $url_var=$exam_id;
        if(isset($this->request->data['Addquestion']['limit']))
        $url_var.="/limit:".$this->request->data['Addquestion']['limit'];
        if(isset($this->request->data['Addquestion']['page']))
        $url_var.="/page:".$this->request->data['Addquestion']['page'];
        if(isset($this->request->data['Addquestion']['keyword']))
        $url_var.="/keyword:".$this->request->data['Addquestion']['keyword'];
        return $this->redirect(array('action' =>"index/$url_var"));
    }
}

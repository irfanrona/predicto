<?php echo $this->Html->css('/Admin/css/bootstrap-multiselect');
echo $this->fetch('css');
echo $this->Html->script('/Admin/js/bootstrap-multiselect');
echo $this->fetch('script');?>
<script type="text/javascript">
    $(document).ready(function(){
    $('#post_req').validationEngine();
    $('.multiselect').multiselect();
});
</script>
<div class="container">
<div class="row">
<?php echo $this->Session->flash();?>
    <div class="col-md-12">    
        <div class="panel panel-default mrg">
	    <div class="panel-heading"><div class="widget-modal"><h4 class="widget-modal-title">Students <span>Wallet</span><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button></h4></div></div>            
                <div class="panel-body">
                <?php echo $this->Form->create('Student', array( 'controller' => 'Student','name'=>'post_req','id'=>'post_req','class'=>'form-horizontal'));?>
                <?php foreach ($Student as $k=>$post): $id=$post['Student']['id'];$form_no=$k+1;?>
                    <div class="panel panel-default">
                        <div class="panel-heading"><strong class="text-danger"><small>Transaction Form <?php echo$form_no?></small></strong></div>
			    <div class="panel-body">
				<div class="form-group">
				    <label for="group_name" class="col-sm-3 control-label"><small>Email</small></label>
				    <div class="col-sm-9">
					<?php echo h($post['Student']['email']);?>
				    </div>
				</div>
				<div class="form-group">
				    <label for="group_name" class="col-sm-3 control-label"><small>Name</small></label>
				    <div class="col-sm-9">
					<?php echo h($post['Student']['name']);?>
				    </div>
				</div>
				<div class="form-group">
				    <label for="group_name" class="col-sm-3 control-label"><small>Mobile</small></label>
				    <div class="col-sm-9">
					<?php echo h($post['Student']['phone']);?>
				    </div>
				</div>
				<div class="form-group">
				    <label for="group_name" class="col-sm-3 control-label"><small>Balance</small></label>
				    <div class="col-sm-9">
					<?php echo (empty($post['Wallet']['balance'])) ? "0.00" : $post['Wallet']['balance'];?>
				    </div>
				</div>
				<div class="form-group">
				    <label for="group_name" class="col-sm-3 control-label"><small>Amount</small></label>
				    <div class="col-sm-3">
					<?php echo $this->Form->input('amount',array('name'=>"data[Student][$id][amount]",'label' => false,'class'=>'form-control input-sm validate[required,custom[number]]','placeholder'=>'Amount','autocomplete'=>'off','div'=>false));?>
				    </div>
				</div>
				<div class="form-group">
				    <label for="group_name" class="col-sm-3 control-label"><small>Action</small></label>
					<div class="col-sm-3">
					    <?php echo $this->Form->select('action',array("Added"=>"ADD","Deducted"=>"DEDUCT"),array('name'=>"data[Student][$id][action]",'empty'=>"Please Select",'label' => false,'class'=>'form-control input-sm validate[required]','div'=>false));?>
					</div>
				</div>
				<div class="form-group">
				    <label for="group_name" class="col-sm-3 control-label"><small>Remarks</small></label>
				    <div class="col-sm-3">
					<?php echo $this->Form->textarea('remarks',array('name'=>"data[Student][$id][remarks]",'label' => false,'class'=>'form-control input-sm validate[required]','placeholder'=>'Remarks','div'=>false));?>
				    </div>
				</div>
				<div class="form-group text-left">
				    <div class="col-sm-offset-3 col-sm-10">
					<?php echo $this->Form->input('id', array('name'=>"data[Student][$id][id]",'value'=>$post['Student']['id'],'type' => 'hidden'));?>
				    </div>
				</div>
			    </div>
		    </div>				
                    <?php endforeach; ?>
                        <?php unset($post); ?>
                        <div class="form-group text-left">
                        <div class="col-sm-offset-3 col-sm-10">                            
                            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-plus-sign"></span> Submit</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php echo $this->Session->flash();?>
<div class="row">
	<?php echo $this->element('pagination',array('IsSearch'=>'No','IsDropdown'=>'No'));
	$page_params = $this->Paginator->params();
	$limit = $page_params['limit'];
	$page = $page_params['page'];
	$serial_no = 1*$limit*($page-1)+1;?>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
			<div class="widget">
				<h4 class="widget-title">Transaction <span>History</span></h4>
			</div>
		</div>		
			<div class="table-responsive">
				<table class="table table-striped">
				<tr>
					<th>S.No.</th>
					<th>Credit</th>
					<th>Debit</th>
					<th>Balance</th>
					<th>Date & Time</th>
					<th>Payment Through</th>
					<th width="400">Remarks</th>
				</tr>
				<?php foreach($Transactionhistory as $post):?>
				<tr>
					<td><?php echo$serial_no++;?></td>
					<td><?php echo$post['Transactionhistory']['in_amount'];?></td>
					<td><?php echo$post['Transactionhistory']['out_amount'];?></td>
					<td><?php echo$post['Transactionhistory']['balance'];?></td>
					<td><?php echo$this->Time->format('d M, Y [H:i]',$post['Transactionhistory']['date']);?></td>
					<td><?php echo$payment_type_arr[$post['Transactionhistory']['type']];?></td>
					<td><?php echo h($post['Transactionhistory']['remarks']);?></td>
				</tr>
				<?php endforeach;unset($post);?>
				</table>
			</div>
		</div>
	</div>
</div>
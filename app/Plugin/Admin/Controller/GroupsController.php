<?php
class GroupsController extends AdminAppController {
    public $helpers = array('Html', 'Form','Session','Paginator');
    public $components = array('Session','Paginator','search-master.Prg');
    public $presetVars = true;
    var $paginate = array('limit'=>20,'maxLimit'=>500,'page'=>1,'order'=>array('Group.group_name'=>'asc'));
    public function index()
    {
        $this->Prg->commonProcess();
        $this->Paginator->settings = $this->paginate;
        $this->Paginator->settings['conditions'] = $this->Group->parseCriteria($this->Prg->parsedParams());
        $this->set('Group', $this->Paginator->paginate());        
    }    
    public function add()
    {
        $this->layout = null;
        if ($this->request->is('post'))
        {
            $this->Group->create();
            try
            {
                if ($this->Group->save($this->request->data))
                {
                    $this->Session->setFlash('Your Group has been saved.','flash',array('alert'=>'success'));
                    return $this->redirect(array('action' => 'index'));
                }
            }
            catch (Exception $e)
            {
                $this->Session->setFlash('Group Name already exist.','flash',array('alert'=>'danger'));
                return $this->redirect(array('action' => 'index'));
            }
        }
    }
    public function edit($id = null)
    {
        $this->layout = null;
        if (!$id)
        {
            throw new NotFoundException(__('Invalid post'));
        }
        $ids=explode(",",$id);
        $post=array();
        foreach($ids as $id)
        {
            $post[]=$this->Group->findByid($id);
        }
        $this->set('Group',$post);
        if (!$post)
        {
            throw new NotFoundException(__('Invalid post'));
        }
        if ($this->request->is(array('post', 'put')))
        {
            $this->Group->id = $id;
            try
            {
                foreach($this->data['Group'] as $key => $value)
                {
                    if ($this->Group->save($this->request->data['Group'][$key]))
                    {
                        $this->Session->setFlash('Your Group has been updated.','flash',array('alert'=>'success'));                        
                    }
                }
                return $this->redirect(array('action' => 'index'));
            }
            catch (Exception $e)
            {
                $this->Session->setFlash('Group Name already exist.','flash',array('alert'=>'danger'));
                return $this->redirect(array('action' => 'index'));
            }
        }
        if (!$this->request->data)
        {
            $this->request->data = $post;
        }
    }    
    public function deleteall()
    {
        if ($this->request->is('post'))
        {
            foreach($this->data['Group']['id'] as $key => $value)
            {
                $this->Group->delete($value);
            }
            $this->Session->setFlash('Your Group has been deleted.','flash',array('alert'=>'success'));
        }        
        $this->redirect(array('action' => 'index'));
    }
}

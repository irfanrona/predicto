<?php
class ContentsController extends AdminAppController {
    public $helpers = array('Html','Form','Session');
    public $components = array('Session');
    public function index()
    {
        
    }
    public function weblogo()
    {
        if ($this->request->is('post'))
        {
            try
            {
                $this->Content->id = 1;
                if ($this->Content->save($this->request->data))
                {
                    $this->Session->setFlash('Your logo has been saved.','flash',array('alert'=>'success'));
                    return $this->redirect(array('action' => 'weblogo'));
                }
            }
            catch (Exception $e)
            {
                $this->Session->setFlash('File type mismatch','flash',array('alert'=>'danger'));
                return $this->redirect(array('action' => 'weblogo'));
            }
        }
    }
    public function weblogodel()
    {
        $post=$this->Content->findById(1);
        $this->loadModel('Configuration');
        $userResult=array('id'=>1,'photo'=>'');
        $file=APP.WEBROOT_DIR.DS.'img'.DS.$post['Content']['photo'];
        if($this->Configuration->save($userResult))
        {
            if(file_exists($file))
            {
                unlink($file);
            }
            $this->Session->setFlash('Your logo has been deleted.','flash',array('alert'=>'danger'));
            return $this->redirect(array('action' => 'weblogo'));
        }
        $this->Session->setFlash('Something wrong!.','flash',array('alert'=>'danger'));
        return $this->redirect(array('action' => 'weblogo'));
    }
}

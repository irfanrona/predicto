<?php
App::uses('CakeEmail', 'Network/Email');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
class ForgotsController extends AdminAppController
{
    public function beforeFilter()
    {
        parent::beforeFilter();        
    }
    public function password()
    {
        if ($this->request->is(array('post', 'put')))
        {
            $email=$this->request->data['Forgot']['email'];
            if($this->Forgot->find('count',array('conditions'=>array('Forgot.email'=>$email)))==0)
            {
                $this->Session->setFlash('Email id is not registered in system','flash', array('alert'=> 'danger'));
                $this->Redirect(array('controller' => 'Forgots', 'action' => 'password'));
            }
            else
            {
                $userValue=$this->Forgot->find('first',array('conditions'=>array('Forgot.email'=>$email)));
                $passwordHasher = new SimplePasswordHasher(array('hashType' => 'sha256'));
                $pass=rand();
                $password=$passwordHasher->hash($pass);
                $this->Forgot->save(array('id'=>$userValue['Forgot']['id'],'password'=>$password));
                $Email = new CakeEmail();
                $Email->from(array($this->siteEmail =>$this->siteName));
                $Email->to($userValue['Forgot']['email']);
                $Email->subject('Forgot Password');
                $Email->send("You have reset your password. your password is $pass");
                $this->Session->setFlash('Your password is reset & sent to your registered email id','flash', array('alert'=> 'success'));
                $this->Redirect(array('controller' => 'Forgots', 'action' => 'password'));
            }
        }
    }
    public function username()
    {
        if ($this->request->is(array('post', 'put')))
        {
            $email=$this->request->data['Forgot']['email'];
            if($this->Forgot->find('count',array('conditions'=>array('Forgot.email'=>$email)))==0)
            {
                $this->Session->setFlash('Email id is not registered in system','flash', array('alert'=> 'danger'));
                $this->Redirect(array('controller' => 'Forgots', 'action' => 'password'));
            }
            else
            {
                $userValue=$this->Forgot->find('first',array('conditions'=>array('Forgot.email'=>$email)));
                $userName=$userValue['Forgot']['username'];
                $Email = new CakeEmail();
                $Email->from(array($userValue['Forgot']['email'] =>$this->siteName));
                $Email->to($this->siteEmail);
                $Email->subject('Forgot User Name');
                $Email->send("You have forgot User Name. your username is $userName");
                $this->Session->setFlash('Your username is sent to your registered email id','flash', array('alert'=> 'success'));
                $this->Redirect(array('controller' => 'Forgots', 'action' => 'password'));
            }
        }
    }
}

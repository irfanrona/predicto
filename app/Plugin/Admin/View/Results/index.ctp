<?php echo $this->Html->css('/Admin/css/bootstrap-multiselect');
echo $this->fetch('css');
echo $this->Html->script('/Admin/js/bootstrap-multiselect');
echo $this->fetch('script');?>
<script type="text/javascript">
    $(document).ready(function(){
    $('#post_req').validationEngine();
    $('.multiselect1').multiselect();
});
</script>
<?php echo $this->Session->flash();?>
	<div class="row">	
		<div class="col-md-12">
		<?php if(!isset($studentId)){?>
			<div class="panel panel-default">
				<div class="panel-heading">
				    <div class="widget">
				    <h4 class="widget-title">Exam <span>Wise</span></h4>
				    </div>
				</div>
				<div class="panel-body">
					<?php echo $this->Form->create('Result',array('class'=>'form-horizontal'));?>
					<div class="form-group">
					<label for="subject_name" class="col-sm-3 control-label"><small>Exam Name</small></label>
						<div class="col-sm-6">
						<?php echo $this->Form->select('id',$examOptions,array('value'=>$examId,'empty'=>'Please Select Exam','onChange'=>'this.form.submit();','class'=>'input-sm'));
						echo $this->Form->end();?>
						</div>
					</div>					
					<?php if($examId!=null){?>
					<div class="row">
						<div class="text-center"><h4><?php echo h($post['Exam']['name']);?>&nbsp;Exam Report</h4></div>
						<div class="col-md-4 text-center text-danger"><strong>Max Mark : </strong><strong><?php echo$post['Result']['total_marks'];?></strong></div>
						<div class="col-md-4 text-center text-danger"><strong>Exam Duration : </strong><strong><?php echo $this->Function->secondsToWords($post['Exam']['duration']*60);?></strong></div>
						<div class="col-md-4 text-center text-danger"><strong>Total Question :</strong> <strong><?php echo$post['Result']['total_question'];?></strong></div>
					</div>
					
					<div class="table-responsive">
						<table class="table table-striped table-bordered">
							<tr>
								<th>Rank</th>
								<th>Student Name</th>
								<th>Mark Obtained</th>
								<th>Question Attempted</th>
								<th>Total Time</th>
								<th>Percentage</th>
							</tr>
							<?php foreach($examResult as $rank=>$examValue):
							$url=$this->Html->url(array('controller'=>'Results'));
							$id=$examValue['Result']['id'];?>
							<tr>
								<td><?php echo++$rank;?></td>
								<td><?php echo $this->Html->link(h($examValue['Student']['name']),'#',array('onclick'=>"show_modal('$url/View/$id');",'escape'=>false));?></td>
								<td><?php echo$examValue['Result']['obtained_marks'];?></td>
								<td><?php echo$examValue['Result']['total_answered'];?></td>
								<td><?php echo$this->Function->secondsToWords($this->Time->fromString($examValue['Result']['end_time'])-$this->Time->fromString($examValue['Result']['start_time']));?></td>
								<td><?php echo$examValue['Result']['percent'];?></td>
							</tr>
							<?php endforeach;unset($examValue);?>
						</table>
						</div>
					<?php }?>
					</div>	
			</div>
				<div class="panel panel-default">
				<div class="panel-heading">
				    <div class="widget">
				    <h4 class="widget-title">Student <span>Wise</span></h4>
				    </div>
				</div>
				<div class="panel-body">	
					<?php echo $this->Form->create('Result',array('class'=>'form-horizontal'));?>
					<div class="form-group">
					<label for="subject_name" class="col-sm-3 control-label"><small>Name or Enrolment Number</small></label>
						<div class="col-sm-3">
						<?php echo $this->Form->input('name',array('label' => false,'class'=>'form-control input-sm]','placeholder'=>'Name or Enrolment Number','div'=>false));?>				
						</div>
					</div>
					<div class="form-group">
					<label for="subject_name" class="col-sm-3 control-label"><small>Student Group</small></label>
						<div class="col-sm-3">
						<?php echo $this->Form->select('StudentGroup.group_name',$group_id,array('multiple'=>true,'label' => false,'class'=>'form-control multiselect1 validate[required]','placeholder'=>'Student Group','div'=>false));?>
						</div>
					</div>
					
					<div class="form-group text-left">
						<div class="col-sm-offset-3 col-sm-10">
							<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-search"></span> Search</button>
						</div>
					</div>		
				</div>			
					<?php echo $this->Form->end();?>
					<?php if(isset($studentDetails)){?>
						<div class="table-responsive">
						<table class="table table-striped table-bordered">
							<tr>
								<th>Student Name</th>
								<th>Enrolment Number</th>
								<th>Group</th>						
							</tr>
							<?php foreach($studentDetails as $studentValue):
							$url=$this->Html->url(array('controller'=>'Results'));
							?>
							<tr>
								<td><?php echo $this->Html->link(h($studentValue['Student']['name']),array('action'=>'index',$studentValue['Student']['id']));?></td>
								<td><?php echo h($studentValue['Student']['enroll']);?></td>
								<td><?php foreach($studentValue['Group'] as $groupName):
									echo h($groupName['group_name']);?> |
									<?php endforeach;unset($groupName);?></td>						
							</tr>
							<?php endforeach;unset($studentValue);?>
						</table>
						</div>
					<?php }}?>
					<?php if(isset($examResult) && isset($studentId)){?>
						<div class="table-responsive">
						<table class="table table-striped table-bordered">
							<tr>
								<th>Exam Name</th>
								<th>Date</th>
								<th>Marks Obtained</th>
								<th>Question Attempted</th>
								<th>Duration</th>
								<th>View Details</th>
							</tr>
							<?php foreach($examResult as $rank=>$examValue):
							$url=$this->Html->url(array('controller'=>'Results'));
							$id=$examValue['Result']['id'];?>
							<tr>
								<td><?php echo h($examValue['Exam']['name']);?></td>
								<td><?php echo$this->Time->format('d-m-Y',$examValue['Result']['start_time']);?></td>
								<td><?php echo$examValue['Result']['obtained_marks'];?></td>
								<td><?php echo$examValue['Result']['total_answered'];?></td>
								<td><?php echo$this->Function->secondsToWords($this->Time->fromString($examValue['Result']['end_time'])-$this->Time->fromString($examValue['Result']['start_time']));?></td>
								<td><?php echo $this->Html->link('View','#',array('onclick'=>"show_modal('$url/View/$id');",'escape'=>false));?></td>
							</tr>
							<?php endforeach;unset($examValue);?>
						</table>
						</div>
					<?php }?>
				
			</div>
	</div>
	</div>
<div class="modal fade" id="targetModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-content">    
  </div>
</div>
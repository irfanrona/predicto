<?php echo $this->Form->create(array('name'=>'searchfrm','action' => "index/$exam_id"));?>
		<div class="row mrg">
		    <div  class="col-md-3">
			<?php
			echo $this->Form->input('subject_id',array('options'=>array($subjectId),'empty'=>'(Please Select)','class'=>'form-control','id'=>'subjectId','div'=>false,'label'=>false));?>
		    </div>
		    <div  class="col-md-3">
			<?php echo $this->Form->input('qtype_id',array('options'=>array($qtypeId),'empty'=>'(Please Select)','class'=>'form-control','div'=>false,'label'=>false));?>
		    </div>
		    <div  class="col-md-2">
			<?php echo $this->Form->input('diff_id',array('options'=>array($diffId),'empty'=>'(Please Select)','class'=>'form-control','div'=>false,'label'=>false));?>
		    </div>
		    <div  class="col-md-3">
			<button type="submit" class="btn btn-success"><span class="fa fa-search"></span> Search</button>
			<?php echo$this->Html->link('<span class="fa fa-refresh"></span>&nbsp;Reset',array('controller'=>'Addquestions','action'=>'index',$exam_id),array('class'=>'btn btn-warning','escape'=>false));?>
		    </div>
		</div>
		<?php echo$this->Form->end();?>
<div class="row">
    <div class="col-md-12">
        <div class="btn-group">
            <?php echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span>&nbsp;Add To Exam','#',array('name'=>'add','id'=>'add','onclick'=>"all_question('add');",'escape'=>false,'class'=>'btn btn-success'));?>
            <?php echo $this->Html->link('<span class="glyphicon glyphicon-trash"></span>&nbsp;Delete To Exam','#',array('name'=>'deleteallfrm','id'=>'deleteallfrm','onclick'=>"all_question('delete');",'escape'=>false,'class'=>'btn btn-danger'));?>
            <?php echo $this->Html->link('<span class="glyphicon glyphicon-arrow-left"></span>&nbsp;Back To Exam',array('controller' => 'Exams','action'=>'index'),array('escape' => false,'class'=>'btn btn-info'));?>
        </div>
    </div>
        <?php echo $this->element('pagination');
        $page_params = $this->Paginator->params();
        $limit = $page_params['limit'];
        $page = $page_params['page'];
        $serial_no = 1*$limit*($page-1)+1;?>
        <?php echo $this->Form->create(array('name'=>'deleteallfrm','controller'=>'Addquestion','action'=>'adddelete'));?>
</div>
<?php echo $this->Session->flash();?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
			<div class="widget">
				<h4 class="widget-title">Add <span>Questions</span></h4>
			</div>
		</div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <tr>
                            <th><?php echo $this->Form->checkbox('checkbox', array('value'=>'deleteall','name'=>'selectAll','label'=>false,'id'=>'selectAll','hiddenField'=>false));?></th>
                            <th><?php echo $this->Paginator->sort('id', 'S.No.', array('direction' => 'desc'));?></th>
                            <th><?php echo $this->Paginator->sort('Subject.subject_name', 'Subject', array('direction' => 'asc'));?></th>
                            <th><?php echo $this->Paginator->sort('Qtype.question_type', 'Type', array('direction' => 'asc'));?></th>
                            <th><?php echo $this->Paginator->sort('question', 'Body of Question', array('direction' => 'asc'));?></th>
                            <th><?php echo $this->Paginator->sort('Diff.diff_level', 'Type', array('direction' => 'asc'));?></th>
                            <th><?php echo $this->Paginator->sort('marks', 'Marks', array('direction' => 'asc'));?></th>
                            <th><?php echo $this->Paginator->sort('hint', 'Hint/Explanation', array('direction' => 'asc'));?></th>
                            <th>Action</th>
                        </tr>
                        <?php foreach ($Addquestion as $post):
                        $id=$post['Addquestion']['id'];?>
                        <tr>
                            <td><?php echo $this->Form->checkbox(false,array('value' => $post['Addquestion']['id'],'name'=>'data[Addquestion][id][]','id'=>"DeleteCheckbox$id",'class'=>'chkselect'));?></td>
                            <td><?php echo $serial_no++; ?></td>
                            <td><?php echo h($post['Subject']['subject_name']); ?></td>
                            <td><?php echo h($post['Qtype']['question_type']); ?></td>
                            <td><?php echo str_replace("<script","",($post['Addquestion']['question'])); ?></td>
                            <td><?php echo h($post['Diff']['diff_level']); ?></td>
                            <td><?php echo h($post['Addquestion']['marks']); ?></td>
                            <td><?php echo h($post['Addquestion']['hint']); ?></td>
                            <td>
                            <?php $is_question=false;                            
                            foreach($ExamQuestion as $eq):
                            if($eq['ExamQuestion']['question_id']==$id){
                            $is_question=true;break;}
                            $is_question=false;
                            endforeach;
                            if($is_question==true)                            
                            echo $this->Html->Link('<span class="glyphicon glyphicon-trash"></span>&nbsp;Delete To Exam','#',array('onclick'=>"single_question('delete','$id');",'class'=>'btn btn-danger','escape'=>false));
                            if($is_question==false)
                            echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span>&nbsp;Add To Exam','#',array('onclick'=>"single_question('add','$id');",'escape'=>false,'class'=>'btn btn-success'));                            
                            unset($eq);?>
                            </td>
                        </tr>
                        <?php endforeach;?>
                        <?php unset($post);?>
                        </table>
                </div>
        </div>
    </div>
</div>
<?php echo $this->Form->input('action',array('name'=>'action','type'=>'hidden','value'=>''));
echo $this->Form->input('exam_id',array('type'=>'hidden','value'=>$exam_id));
echo $this->Form->input('limit',array('type'=>'hidden','value'=>$limit));
echo $this->Form->input('page',array('type'=>'hidden','value'=>$page));
echo $this->Form->input('keyword',array('type'=>'hidden','value'=>$this->params['keyword']));
echo $this->Form->input('subject_id',array('type'=>'hidden','value'=>$this->params['subject_id']));
echo $this->Form->input('qtype_id',array('type'=>'hidden','value'=>$this->params['qtype_id']));
echo $this->Form->input('diff_id',array('type'=>'hidden','value'=>$this->params['diff_id']));
echo $this->Form->end();?>
<?php echo $this->element('pagination');?>
<div class="modal fade" id="targetModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-content">        
  </div>
</div>
<?php echo $this->Session->flash();?>
		<div class="row">	
			<div class="col-md-3">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="widget">
						<h4 class="widget-title">Exam <span>Statistic</span></h4>
						</div>
					</div>
					<table class="table">
						<tr>
							<td>In Progress</td>
							<td>__________</td>
							<td><?php echo$total_inprogress_exam;?></td>
						</tr>
						<tr>
							<td>Upcoming</td>
							<td>__________</td>
							<td><?php echo$total_upcoming_exam;?></td>
						</tr>
						<tr>
							<td>Completed</td>
							<td>__________</td>
							<td><?php echo$total_completed_exam;?></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</div>
			</div>
			
			<div class="col-md-9">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="widget">
						<h4 class="widget-title">Up Coming <span>Exams</span></h4>
						</div>
					</div>					
					<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<tr>
							<th>Date</th>
							<th>Exam Name</th>
							<th>Group</th>
							<th>Marks</th>
							<th>Duration</th>
						</tr>
						<tr>
						<?php $i=0; foreach($UpcomingExam as $post):$i++;?>
						<tr>
							<td><?php echo$this->Time->format('d-m-Y',$post['Exam']['start_date']);?></td>
							<td><?php echo h($post['Exam']['name']);?></td>
							<td><?php foreach($post['Group'] as $k=>$groupName):
                            echo h($groupName['group_name']);?> |
                            <?php endforeach;unset($groupName);?></td>
							<td><?php echo $post['Exam']['total_marks'];?></td>
							<td><?php echo $this->Function->secondsToWords($post['Exam']['duration']*60);?></td>
						</tr>
						<?php endforeach;?>
						<?php unset($post);?>
						<?php for($j=$i;$j<3;$j++):?>
						<tr><td colspan="5">&nbsp;</td></tr>
						<?php endfor;?>
						<?php unset($i);unset($j);?>
					</table>
					</div>					
				</div> 
			</div>
			<?php echo$this->Form->create('Results',array('controller'=>'Results','action'=>'index','name'=>'post_req','id'=>'post_req'));?>
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="widget">
						<h4 class="widget-title">Recent <span>Exam Results</span></h4>
						</div>
					</div>
					<div class="table-responsive">
					<table class="table table-bordered">
						<tr>
							<th>Exam</th>
							<th>Overall Result</th>
							<th>Student Stats</th>							
						</tr>
						<?php foreach($recentExamResult as $recentValue):
						$id=$recentValue['RecentExam']['Exam']['id'];?>
							<tr>
								<td><strong class="text-danger"><?php echo h($recentValue['RecentExam']['Exam']['name']);?></strong><br/>
								From: <strong class="text-danger"><?php echo$this->Time->format('d M, Y H:i A',$recentValue['RecentExam']['Exam']['start_date']);?></strong><br/>
								To: <strong class="text-danger"><?php echo$this->Time->format('d M, Y H:i A',$recentValue['RecentExam']['Exam']['end_date']);?></strong><br/>
								<?php echo$this->Html->link('Details','#',array('onclick'=>"examResult($id);"));?>
								</td>
								<td>
									<div class="chart">
									<div id="mywrapperor<?php echo$id;?>"></div>
									<?php echo $this->HighCharts->render("My Chartor$id");?>
									</div>
								</td>
								<td>
									<div class="chart">
									<div id="mywrapperss<?php echo$id;?>"></div>
									<?php echo $this->HighCharts->render("My Chartss$id");?>
									</div>
								</td>
							</tr>
							<?php endforeach;?>
							<?php unset($recentValue);?>
					</table>
					</div>
				</div> 
			</div>
			<?php echo$this->Form->hidden('Result',array('name'=>'id','value'=>''));?>
			<?php echo$this->Form->end(null);?>
			<div class="col-md-12"><div class="row">
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="widget">
						<h4 class="widget-title">Student <span>Group Graph</span></h4>
						</div>
					</div>
					<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<tr>
							<td>
							<div class="chart">
							<div id="mywrapperd2"></div>
							<?php echo $this->HighCharts->render("My Chartd2");?>
							</div>
							</td>
							
					</table>
					</div>
				</div> 
			</div>
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="widget">
						<h4 class="widget-title">Student <span>Statistic Table</span></h4>
						</div>
					</div>
					<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<tr>
							<th>Group Name</th>
							<th>Total Students</th>
							<th>Total Active</th>
							<th>Total Pending</th>
							<th>Total Suspended</th>
						</tr>
						<?php foreach($studentStatitics as $studentValue):?>
						<tr>
							<td><?php echo h($studentValue['GroupName']['name']);?></td>
							<td><?php echo$studentValue['GroupName']['total_student'];?></td>
							<td><?php echo$studentValue['GroupName']['active'];?></td>
							<td><?php echo$studentValue['GroupName']['pending'];?></td>
							<td><?php echo$studentValue['GroupName']['suspend'];?></td>
						</tr>
						<?php endforeach;unset($studentValue);?>												
					</table>
					</div>
				</div> 
			</div>
			</div>
			</div>
			<div class="col-md-12"><div class="row">
			<div  class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="widget">
						<h4 class="widget-title">Question Bank <span>Subject Wise</span></h4>
						</div>
					</div>				
					<div class="chart">
					<div id="piewrapperqc"></div>
					<?php echo $this->HighCharts->render("Pie Chartqc");?>
					</div>
				</div>
			</div>
			<div  class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="widget">
						<h4 class="widget-title">Difficulty Level <span>Chart of Question</span></h4>
						</div>
					</div>				
					<div class="chart">
					<div id="mywrapperdl"></div>
					<?php echo $this->HighCharts->render("My Chartdl");?>
					</div>
				</div>
			</div>
			</div></div>
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="widget">
						<h4 class="widget-title">Question <span>Count Table</span></h4>
						</div>
					</div>	
					<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<tr>
							<th>Bank Name</th>
							<th>Total Question</th>
							<th>Total Easy</th>
							<th>Total Normal</th>
							<th>Total Difficult</th>							
						</tr>
						<?php foreach($Subject as $sd):?>
						<tr><td><?php echo$subject_name= h($sd['Subject']['subject_name']);?></td>
						<td><?php echo$DifficultyDetail[$subject_name]['total_question'];?></td>
						<?php $i=0; foreach($DiffLevel as $diff):?>
						<td><?php echo$DifficultyDetail[$subject_name][$i];?></td>
						<?php $i++;endforeach;?>
						</tr>
						<?php endforeach;?>
						<?php unset($sd);?>	
							
					</table>
					</div>
				</div> 
			</div>
		</div>
		<script type="text/javascript">
	function examResult(id)
	{
		document.post_req.id.value=id;
		document.post_req.submit();
	}
</script>
<?php
class QuestionsController extends AdminAppController {
    public $helpers = array('Html', 'Form','Session','Paginator','Tinymce');
    public $components = array('Session','Paginator','search-master.Prg');
    public $presetVars = true;
    var $paginate = array('limit'=>20,'maxLimit'=>500,'page'=>1,'order'=>array('Question.subject_name'=>'asc'));
    public function index()
    {
        $this->Prg->commonProcess();
        $this->Paginator->settings = $this->paginate;
        $this->Paginator->settings['conditions'] = $this->Question->parseCriteria($this->Prg->parsedParams());
        $this->set('Question', $this->Paginator->paginate());        
    }    
    public function add()
    {
        $this->loadModel('Qtype');
        $this->loadModel('Subject');
        $this->loadModel('Diff');
        $this->set('qtype_id', $this->Qtype->find('list',array('fields'=>array('id','question_type'))));
        $this->set('subject_id', $this->Subject->find('list',array('fields'=>array('id','subject_name'))));
        $this->set('diff_id', $this->Diff->find('list',array('fields'=>array('id','diff_level'))));        
        if ($this->request->is('post'))
        {
            $this->Question->create();
            try
            {
                if(is_array($this->request->data['Question']['answer']))
                $this->request->data['Question']['answer']=implode(",",$this->request->data['Question']['answer']);
                if($this->request->data['Question']['qtype_id']==1 && $this->request->data['Question']['answer']==null)
                {
                    $this->Session->setFlash('Please Check any answer!','flash',array('alert'=>'danger'));
                    return $this->redirect(array('action' => 'index'));
                }
                elseif($this->request->data['Question']['qtype_id']==2 && $this->request->data['Question']['true_false']==null)
                {
                    $this->Session->setFlash('Please select true or false!','flash',array('alert'=>'danger'));
                    return $this->redirect(array('action' => 'index'));
                }
                elseif($this->request->data['Question']['qtype_id']==3 && $this->request->data['Question']['fill_blank']==null)
                {
                    $this->Session->setFlash('Please fill blank space!','flash',array('alert'=>'danger'));
                    return $this->redirect(array('action' => 'index'));
                }
                else
                {
                    if ($this->Question->save($this->request->data))
                    {
                        $this->Session->setFlash('Your Question has been saved.','flash',array('alert'=>'success'));
                        return $this->redirect(array('action' => 'index'));
                    }
                }
            }
            catch (Exception $e)
            {
                $this->Session->setFlash('Question Error.','flash',array('alert'=>'danger'));
                return $this->redirect(array('action' => 'index'));
            }
        }
    }
    public function edit($id = null)
    {
        $this->loadModel('Qtype');
        $this->loadModel('Subject');
        $this->loadModel('Diff');
        $this->set('qtype_id', $this->Qtype->find('list',array('fields'=>array('id','question_type'))));
        $this->set('subject_id', $this->Subject->find('list',array('fields'=>array('id','subject_name'))));
        $this->set('diff_id', $this->Diff->find('list',array('fields'=>array('id','diff_level'))));
        if (!$id)
        {
            throw new NotFoundException(__('Invalid post'));
        }
        $ids=explode(",",$id);
        $post=array();
        foreach($ids as $id)
        {
            $post[]=$this->Question->findByid($id);
        }
        $this->set('Question',$post);
        if (!$post)
        {
            throw new NotFoundException(__('Invalid post'));
        }
        if ($this->request->is(array('post', 'put')))
        {
            $this->Question->id = $id;
            try
            {
                foreach($this->data['Question'] as $key => $value)
                {
                    if(is_array($this->request->data['Question'][$key]['answer']))
                    $this->request->data['Question'][$key]['answer']=implode(",",$this->request->data['Question'][$key]['answer']);
                    if($this->request->data['Question'][$key]['qtype_id']==1 && $this->request->data['Question'][$key]['answer']==null)
                    {
                        $this->Session->setFlash('Please Check any answer!','flash',array('alert'=>'danger'));
                        return $this->redirect(array('action' => 'index'));
                    }
                    elseif($this->request->data['Question'][$key]['qtype_id']==2 && $this->request->data['Question'][$key]['true_false']==null)
                    {
                        $this->Session->setFlash('Please select true or false!','flash',array('alert'=>'danger'));
                        return $this->redirect(array('action' => 'index'));
                    }
                    elseif($this->request->data['Question'][$key]['qtype_id']==3 && $this->request->data['Question'][$key]['fill_blank']==null)
                    {
                        $this->Session->setFlash('Please fill blank space!','flash',array('alert'=>'danger'));
                        return $this->redirect(array('action' => 'index'));
                    }
                    else
                    {
                        if ($this->Question->save($this->request->data['Question'][$key]))
                        {
                            $this->Session->setFlash('Your Question has been updated.','flash',array('alert'=>'success'));                        
                        }
                    }
                }
                return $this->redirect(array('action' => 'index'));
            }
            catch (Exception $e)
            {
                $this->Session->setFlash('Question Error.','flash',array('alert'=>'danger'));
                return $this->redirect(array('action' => 'index'));
            }
        }
        if (!$this->request->data)
        {
            $this->request->data = $post;
        }
    }    
    public function deleteall()
    {
        if ($this->request->is('post'))
        {
            try
            {
                foreach($this->data['Question']['id'] as $key => $value)
                {
                    $this->Question->delete($value);
                }
                $this->Session->setFlash('Your Question has been deleted.','flash',array('alert'=>'success'));
            }
            catch (Exception $e)
            {
                $this->Session->setFlash('Delete exam first!','flash',array('alert'=>'danger'));
                return $this->redirect(array('action' => 'index'));
            }
        }        
        $this->redirect(array('action' => 'index'));
    }
}
